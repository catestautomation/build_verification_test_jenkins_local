﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SiteCore_Regression_Automation.Pages;
using AshleyAutomationLibrary;
using System.Threading;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium.Interactions;
using System.Globalization;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Edge;

namespace SiteCore_Regression_Automation.Tests
{
    [TestClass]
    [DeploymentItem(@"TestDriver", @"TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]

    public class BVTTest
    {
        CommonFunctions CommonFunctions;
        BillingPage BillingPage;
        CartPage CartPage;
        ConfirmOrderPage ConfirmOrderPage;
        Footer Footer;
        Header Header;
        HomePage HomePage;
        LoginPage LoginPage;
        OrderDetailsPage OrderDetailsPage;
        PaymentPage PaymentPage;
        PayPalPage PayPalPage;
        ProductDetailsPage ProductDetailsPage;
        SearchResultsPage SearchResultsPage;
        ShippingPage ShippingPage;
        StoreLocatorPage StoreLocatorPage;
        ThankYouPage ThankYouPage;
        WelcomePage WelcomePage;

        #region Public Variables

        //IWebDriver driver = null;
        public string status = null;

        #endregion

        [TestMethod]
        [TestCategory("Cart Page")]
        [Priority(1)]
        [Description("CartPage - QuantityUpdate / RemoveItem / RSAID Update / PromoCode / ZipCode Update")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\CartPageBVT.csv", "CartPageBVT#csv", DataAccessMethod.Sequential)]
        // [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "111858", DataAccessMethod.Sequential)]
        public void CartPageUpdate()
        {

            #region TestData
            string ZipCode = TestContext.DataRow["CreditZipCode"].ToString();
            string Quantity = TestContext.DataRow["Quantity"].ToString();
            string RSA_text = TestContext.DataRow["RSA_text"].ToString();
            string RSA_another_txt = TestContext.DataRow["RSA_another_txt"].ToString();
            string promoCode1 = TestContext.DataRow["PromoCode1"].ToString();
            string promoCode2 = TestContext.DataRow["PromoCode2"].ToString();
            string updatedZipCode = TestContext.DataRow["UpdatedZipCode"].ToString();
            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region Pre-requisite

            try
            {
                //Navigate to PDP Page
                ProductDetailsPage.navigateToPDPPage();
                Archive.WaitForPageLoad();

                //Adding Product to Cart
                status = ProductDetailsPage.addToCartPDP(ZipCode, "1");
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.clickPDPCheckOut();
                Archive.WaitForPageLoad();
                TestContext.WriteLog("Click on checkOut button successful");

                #endregion

                #region TestSteps

                status = CommonFunctions.GetEnvironmentURL();

                Archive.WaitForElement(By.XPath("/descendant::span[contains(.,'Proceed to Checkout')]"));
                string subTotal = CartPage.txt_subTotal[0].Text.Replace("$", "");
                double subTotalValue = Convert.ToDouble(subTotal);

                //Update Quantity in Cart Page
                CartPage.updateQuantity(Quantity);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                string updatedSubTotal = CartPage.txt_subTotal[0].Text.Replace("$", "");
                double updatedSubTotalValue = Convert.ToDouble(updatedSubTotal);

                Assert.AreNotEqual(subTotalValue, updatedSubTotalValue, "Quantity is not getting updated");

                //Apply RSA Id in Cart Page
                status = CartPage.enterRSAID(RSA_text);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //Edit RSA Id in Cart Page
                status = CartPage.editRSAID(RSA_another_txt);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //Remove RSA Id in Cart Page
                status = CartPage.removeRSAID();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //Enter PromoCode in Cart Page
                status = CartPage.enterPromoCode(promoCode1);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //Edit, Update and Remove Promocode in Cart Page
                status = CartPage.editAndRemovePromoCode(promoCode2);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //Update Zipcode in Cart Page
                status = CartPage.updateZipCode(updatedZipCode);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //Remove Items in Cart Page
                status = CartPage.removeItems();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                Archive.WaitForElement(By.XPath("//h3[.='Your Shopping Cart Is Empty']"));

            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("Cart Page")]
        [Priority(1)]
        [Description("CartPage - Move To Wish List")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\CartPageBVT.csv", "CartPageBVT#csv", DataAccessMethod.Sequential)]
        // [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "111858", DataAccessMethod.Sequential)]
        public void CartPageMoveToWishList()
        {

            #region TestData
            string ZipCode = TestContext.DataRow["CreditZipCode"].ToString();
            string WishListName = TestContext.DataRow["WishListName"].ToString();
            string username = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region Pre-requisite

            try
            {

                status = CommonFunctions.GetEnvironmentURL();
                //Navigate to PDP Page
                ProductDetailsPage.navigateToPDPPage();
                Archive.WaitForPageLoad();

                //Adding Product to Cart
                status = ProductDetailsPage.addToCartPDP(ZipCode, "1");
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.clickPDPCheckOut();
                Archive.WaitForPageLoad();
                TestContext.WriteLog("Click on checkOut button successful");

                Archive.WaitForElement(By.XPath("/descendant::span[contains(.,'Proceed to Checkout')]"));

                #endregion

                #region TestSteps           
                
                //Move to WIsh List in Cart Page
                status = CartPage.moveToWishList();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //Enter Email, Password and Click Sign In
                status = LoginPage.signIn(username, password);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = CartPage.moveToWishList();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.createAnotherWishList(WishListName);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                Archive.WaitForElement(By.Id("addToWishListMessage"));

            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("Cart Page")]
        [Priority(1)]
        [Description("CartPage - PayPal Express Checkout")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\CartPageBVT.csv", "CartPageBVT#csv", DataAccessMethod.Sequential)]
        // [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "111858", DataAccessMethod.Sequential)]
        public void CartPagePayPalExpressCheckout()
        {

            #region TestData
            string ZipCode = TestContext.DataRow["CreditZipCode"].ToString();
            string paypalUsername = TestContext.DataRow["PayPalUserName"].ToString();
            string paypalPassword = TestContext.DataRow["PayPalPassword"].ToString();
            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region Pre-requisite

            try
            {
                status = CommonFunctions.GetEnvironmentURL();
                //Navigate to PDP Page
                ProductDetailsPage.navigateToPDPPage();
                Archive.WaitForPageLoad();

                //Adding Product to Cart
                status = ProductDetailsPage.addToCartPDP(ZipCode, "1");
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.clickPDPCheckOut();
                Archive.WaitForPageLoad();
                TestContext.WriteLog("Click on checkOut button successful");

                Archive.WaitForElement(By.XPath("/descendant::span[contains(.,'Proceed to Checkout')]"));

                #endregion

                #region TestSteps

                Archive.WaitForElement(By.XPath("//button[contains(@class,'paypal-button')]"));
                status = CartPage.expressPayPalPaymentCheckOut(paypalUsername, paypalPassword);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ConfirmOrderPage.placeOrder();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ThankYouPage.orderConfirmOverlayClick();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion
        }


        [TestMethod]
        [TestCategory("Order History")]
        [Priority(1)]
        [Description("OrderDetailsPage - Order History Profiled")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\OrderHistoryBVT.csv", "OrderHistoryBVT#csv", DataAccessMethod.Sequential)]
        // [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "111858", DataAccessMethod.Sequential)]
        public void OrderDetailsPageOrderHistoryProfiled()
        {

            #region TestData
            string ZipCode = TestContext.DataRow["CreditZipCode"].ToString();
            string username = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            string firstName = TestContext.DataRow["FirstName"].ToString();
            string lastName = TestContext.DataRow["LastName"].ToString();
            Random randomGenerator = new Random();
            double randomInt = randomGenerator.NextDouble();
            string email = "venhatesh" + randomInt + "@abc.com";
            string address = TestContext.DataRow["CreditAddress"].ToString();
            string city = TestContext.DataRow["CreditCity"].ToString();
            string state = TestContext.DataRow["CreditState"].ToString();
            string phone = TestContext.DataRow["Phone"].ToString();
            string creditCardNumber = TestContext.DataRow["CreditCardNumber"].ToString();
            string securityCode = TestContext.DataRow["SecurityCode"].ToString();
            string expMonth = "March (03)";
            string expYear = "2018";
            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region Pre-requisite

            try
            {
                status = CommonFunctions.GetEnvironmentURL();
                //Navigate to PDP Page
                ProductDetailsPage.navigateToPDPPage();
                Archive.WaitForPageLoad();

                //Adding Product to Cart
                status = ProductDetailsPage.addToCartPDP(ZipCode, "1");
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.clickPDPCheckOut();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = CartPage.proceedToCheckout_CartPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //Enter Email, Password and Click Sign In
                status = LoginPage.signIn(username, password);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //Enter Shipping
                status = ShippingPage.addShippingAddress(firstName, lastName, address, city, state, phone, ZipCode, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                // Enter Billing
                status = BillingPage.addBillingAddress(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = PaymentPage.enterCreditPayments(firstName, lastName, creditCardNumber, securityCode, expMonth, expYear);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                // Click Submit Order
                status = ConfirmOrderPage.submitOrder();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ThankYouPage.orderConfirmOverlayClick();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                #endregion

                #region TestSteps

                // Check Order History under My account section
                status = Header.myAccountOption("Order");
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                // Verify Recent Orders link
                Assert.IsTrue(OrderDetailsPage.lnk_recentOrders.Displayed);
                TestContext.WriteLog("'Recent Orders' link is showing");

            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("Order History")]
        [Priority(1)]
        [Description("OrderDetailsPage - Order History Guest")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\OrderHistoryBVT.csv", "OrderHistoryBVT#csv", DataAccessMethod.Sequential)]
        // [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ash
        public void OrderDetailsPageOrderHistoryGuest()
        {

            #region TestData
            string ZipCode = TestContext.DataRow["CreditZipCode"].ToString();
            string firstName = TestContext.DataRow["FirstName"].ToString();
            string lastName = TestContext.DataRow["LastName"].ToString();
            Random randomGenerator = new Random();
            double randomInt = randomGenerator.NextDouble();
            string email = "venhatesh" + randomInt + "@abc.com";
            string address = TestContext.DataRow["CreditAddress"].ToString();
            string city = TestContext.DataRow["CreditCity"].ToString();
            string state = TestContext.DataRow["CreditState"].ToString();
            string phone = TestContext.DataRow["Phone"].ToString();
            string creditCardNumber = TestContext.DataRow["CreditCardNumber"].ToString();
            string securityCode = TestContext.DataRow["SecurityCode"].ToString();
            string expMonth = "March (03)";
            string expYear = "2018";
            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region Pre-requisite

            try
            {
                status = CommonFunctions.GetEnvironmentURL();
                //Navigate to PDP Page
                ProductDetailsPage.navigateToPDPPage();
                Archive.WaitForPageLoad();

                //Adding Product to Cart
                status = ProductDetailsPage.addToCartPDP(ZipCode, "1");
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.clickPDPCheckOut();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = CartPage.proceedToCheckout_CartPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //Enter Email, Password and Click Sign In
                status = LoginPage.clickGuestCheckout();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //Enter Shipping
                status = ShippingPage.addShippingAddress(firstName, lastName, address, city, state, phone, ZipCode, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                // Enter Billing
                status = BillingPage.addBillingAddress(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = PaymentPage.enterCreditPayments(firstName, lastName, creditCardNumber, securityCode, expMonth, expYear);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                // Click Submit Order
                status = ConfirmOrderPage.submitOrder();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ThankYouPage.orderConfirmOverlayClick();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                #endregion

                #region TestSteps

                //Confirmation Number has been taken
                List<IWebElement> ConfirmNumber = ConfirmOrderPage.elm_confirmationNumber.GetDescendants();
                int index = ConfirmNumber.IndexOf(ConfirmNumber.First(x => x.Text.StartsWith("7250")));
                string ConfirmationNumber = ConfirmNumber[index].Text.Replace(" ", "");

                //Clicking on the Order Status
                status = Header.myAccountOption("Order");
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //Entering the Order Placed details for Order status
                status = OrderDetailsPage.checkOrderStatus(ConfirmationNumber, email, ZipCode);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                Archive.WaitForElement(By.XPath("//h2[.='Order Details']"));

            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("Order Submission")]
        [Priority(1)]
        [Description("OrderSubmission – Credit Card Order")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\PaymentPageBVT.csv", "PaymentPageBVT#csv", DataAccessMethod.Sequential)]
        public void VerifyPaymentTypes_CreditCard()
        {
            #region Testdata
            string Category = TestContext.DataRow["Category"].ToString();
            string SubCategory = TestContext.DataRow["SubCategory"].ToString();
            string zipCode = TestContext.DataRow["CreditZipCode"].ToString();
            string userName = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            string creditCardNumber = TestContext.DataRow["CreditCardNumber"].ToString();
            string securityCode = TestContext.DataRow["SecurityCode"].ToString();
            string firstName = TestContext.DataRow["FirstName"].ToString();
            string lastName = TestContext.DataRow["LastName"].ToString();
            string email = TestContext.DataRow["Email"].ToString();
            string address = TestContext.DataRow["CreditAddress"].ToString();
            string city = TestContext.DataRow["CreditCity"].ToString();
            string state = TestContext.DataRow["CreditState"].ToString();
            string phone = TestContext.DataRow["Phone"].ToString();
            string zip = TestContext.DataRow["CreditZipCode"].ToString();
            string expMonth = "March (03)";
            string expYear = "2018";
            string ashleyCardNumber = TestContext.DataRow["AshleyCardNumber"].ToString();
            string financingOption = TestContext.DataRow["FinancingOption"].ToString();
            string paypalUsername = TestContext.DataRow["paypalUsername"].ToString();
            string paypalPassword = TestContext.DataRow["paypalPassword"].ToString();

            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region Pre requisite

            try
            {
                status = CommonFunctions.GetEnvironmentURL();
                //Navigate to PDP Page
                ProductDetailsPage.navigateToPDPPage();
                Archive.WaitForPageLoad();

                status = ProductDetailsPage.addToCartPDP(zipCode);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.clickPDPCheckOut();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = CartPage.proceedToCheckout_CartPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = LoginPage.clickGuestCheckout(); // Continue as Guest
                //status = LoginPage.signIn(userName, password); // Continue as User
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                ShippingPage.addShippingAddress(firstName, lastName, address, city, state, phone, zipCode, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = BillingPage.addBillingAddress(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);
                #endregion

                #region Test Steps

                status = PaymentPage.enterCreditPayments(firstName, lastName, creditCardNumber, securityCode, expMonth, expYear); // Credit Card Payment
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ConfirmOrderPage.submitOrder(); // Credit Card Payment and Ashley Advantage Card Payment
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ThankYouPage.orderConfirmOverlayClick();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

            }
            catch (Exception ex)
            {
                string strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion
        }


        [TestMethod]
        [TestCategory("Order Submission")]
        [Priority(1)]
        [Description("OrderSubmission – Ashley Advantage Card")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\PaymentPageBVT.csv", "PaymentPageBVT#csv", DataAccessMethod.Sequential)]
        public void VerifyPaymentTypes_AshleyAdvantageCard()
        {
            #region Testdata
            string Category = TestContext.DataRow["Category"].ToString();
            string SubCategory = TestContext.DataRow["SubCategory"].ToString();
            string zipCode = TestContext.DataRow["CreditZipCode"].ToString();
            string userName = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            string creditCardNumber = TestContext.DataRow["CreditCardNumber"].ToString();
            string securityCode = TestContext.DataRow["SecurityCode"].ToString();
            string firstName = TestContext.DataRow["FirstName"].ToString();
            string lastName = TestContext.DataRow["LastName"].ToString();
            string email = TestContext.DataRow["Email"].ToString();
            string address = TestContext.DataRow["CreditAddress"].ToString();
            string city = TestContext.DataRow["CreditCity"].ToString();
            string state = TestContext.DataRow["CreditState"].ToString();
            string phone = TestContext.DataRow["Phone"].ToString();
            string zip = TestContext.DataRow["CreditZipCode"].ToString();
            string ashleyCardNumber = TestContext.DataRow["AshleyCardNumber"].ToString();
            string financingOption = TestContext.DataRow["FinancingOption"].ToString();

            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region Pre requisite

            try
            {
                status = CommonFunctions.GetEnvironmentURL();
                //Navigate to PDP Page
                ProductDetailsPage.navigateToPDPPage();
                Archive.WaitForPageLoad();

                status = ProductDetailsPage.addToCartPDP(zipCode);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.clickPDPCheckOut();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = CartPage.proceedToCheckout_CartPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = LoginPage.clickGuestCheckout(); // Continue as Guest
                //status = LoginPage.signIn(userName, password); // Continue as User
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                ShippingPage.addShippingAddress(firstName, lastName, address, city, state, phone, zipCode, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = BillingPage.addBillingAddress(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);
                #endregion

                #region Test Steps

                status = PaymentPage.enterAshleyAdvantageCardPayments(ashleyCardNumber, financingOption); // Ashley Advantage Card Payment
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ConfirmOrderPage.submitOrder(); // Credit Card Payment and Ashley Advantage Card Payment
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ThankYouPage.orderConfirmOverlayClick();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);


            }
            catch (Exception ex)
            {
                string strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion
        }


        [TestMethod]
        [TestCategory("Order Submission")]
        [Priority(1)]
        [Description("OrderSubmission – PayPal Order")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\PaymentPageBVT.csv", "PaymentPageBVT#csv", DataAccessMethod.Sequential)]
        public void VerifyPaymentTypes_Paypal()
        {
            #region Testdata
            string Category = TestContext.DataRow["Category"].ToString();
            string SubCategory = TestContext.DataRow["SubCategory"].ToString();
            string zipCode = TestContext.DataRow["CreditZipCode"].ToString();
            string userName = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            string creditCardNumber = TestContext.DataRow["CreditCardNumber"].ToString();
            string securityCode = TestContext.DataRow["SecurityCode"].ToString();
            string firstName = TestContext.DataRow["FirstName"].ToString();
            string lastName = TestContext.DataRow["LastName"].ToString();
            string email = TestContext.DataRow["Email"].ToString();
            string address = TestContext.DataRow["CreditAddress"].ToString();
            string city = TestContext.DataRow["CreditCity"].ToString();
            string state = TestContext.DataRow["CreditState"].ToString();
            string phone = TestContext.DataRow["Phone"].ToString();
            string zip = TestContext.DataRow["CreditZipCode"].ToString();
            string ashleyCardNumber = TestContext.DataRow["AshleyCardNumber"].ToString();
            string financingOption = TestContext.DataRow["FinancingOption"].ToString();
            string paypalUsername = TestContext.DataRow["paypalUsername"].ToString();
            string paypalPassword = TestContext.DataRow["paypalPassword"].ToString();

            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region Pre requisite

            try
            {
                status = CommonFunctions.GetEnvironmentURL();
                //Navigate to PDP Page
                ProductDetailsPage.navigateToPDPPage();
                Archive.WaitForPageLoad();

                status = ProductDetailsPage.addToCartPDP(zipCode);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.clickPDPCheckOut();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = CartPage.proceedToCheckout_CartPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = LoginPage.clickGuestCheckout(); // Continue as Guest
                //status = LoginPage.signIn(userName, password); // Continue as User
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                ShippingPage.addShippingAddress(firstName, lastName, address, city, state, phone, zipCode, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = BillingPage.addBillingAddress(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);
                #endregion

                #region Test Steps

                status = PaymentPage.payPalPayment(paypalUsername, paypalPassword); // Paypal Payment
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                status = ConfirmOrderPage.placeOrder(); // Paypal Payment Only
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                status = ThankYouPage.orderConfirmOverlayClick();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);


            }
            catch (Exception ex)
            {
                string strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }

            #endregion
        }

        [TestMethod]
        [TestCategory("Home Page")]
        [Priority(1)]
        [Description("Verify able to move login page")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\HomePageBVT.csv", "HomePageBVT#csv", DataAccessMethod.Sequential)]

        public void SignIn()
        {

            #region TestData
            string userName = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            #endregion 

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration

            #endregion

            #region TestSteps
            try
            {


                status = CommonFunctions.GetEnvironmentURL();               

                status = LoginPage.navigateToLoginPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                try
                {
                    Config.driver.Manage().Cookies.DeleteAllCookies();
                    Config.driver.Navigate().Refresh();
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }

                status = LoginPage.signIn(userName, password);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                string login_Title = LoginPage.txt_headers.Text;
                if (login_Title != "My Account")
                    Assert.AreEqual("My Account", login_Title, "The Login Title is Wrong");
                TestContext.WriteLog("My Account title validation is Successful");

            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion

        }

        [TestMethod]
        [TestCategory("Home Page")]
        [Priority(1)]
        [Description("Verify able to create account")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\HomePageBVT.csv", "HomePageBVT#csv", DataAccessMethod.Sequential)]

        public void CreateAccount()
        {
            #region TestData
            string userName = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            string firstName = TestContext.DataRow["FirstName"].ToString();
            string lastName = TestContext.DataRow["LastName"].ToString();
            Random randomGenerator = new Random();
            double randomInt = randomGenerator.NextDouble();
            string email = "venhatesh" + randomInt + "@abc.com";
            #endregion 

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration

            #endregion

            #region TestSteps
            try
            {
                status = CommonFunctions.GetEnvironmentURL();

                status = LoginPage.navigateToLoginPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = LoginPage.createAccount(firstName, lastName, email, password);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                string login_Title = LoginPage.txt_headers.Text;
                if (login_Title != "My Account")
                    Assert.AreEqual("My Account", login_Title, "The Login Title is Wrong");
                TestContext.WriteLog("My Account title validation Successful");


            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("Home Page")]
        [Priority(1)]
        [Description("Verify able to view Find Store Locator")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\HomePageBVT.csv", "HomePageBVT#csv", DataAccessMethod.Sequential)]

        public void VerifyFindStore()
        {
            #region TestData
            string Zip = TestContext.DataRow["CreditZipCode"].ToString();

            #endregion 

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration

            #endregion

            #region TestSteps
            try
            {
                status = CommonFunctions.GetEnvironmentURL();

                status = status.Split('/')[2];

                status = StoreLocatorPage.navigateToFindStore();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = StoreLocatorPage.SendZipCodeandClick(Zip);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                Assert.AreEqual(true, StoreLocatorPage.Search_Result.Displayed);
                Assert.AreEqual(true, StoreLocatorPage.txt_LocatoreResults.Displayed);
                TestContext.WriteLog("Searched result is showing");

            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                //String strfinal = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "\\testresults\\123.png";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion

        }

        [TestMethod]
        [TestCategory("Shipping and Billing Page")]
        [Priority(1)]
        [Description("Shippinh & Billing page validation")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\ShippingAndBillingPageBVT.csv", "ShippingAndBillingPageBVT#csv", DataAccessMethod.Sequential)]
        public void shippingAndBillingPageValidation()
        {
            #region Testdata
            string zipCode = TestContext.DataRow["CreditZipCode"].ToString();
            string firstName = TestContext.DataRow["FirstName"].ToString();
            string lastName = TestContext.DataRow["LastName"].ToString();
            string email = TestContext.DataRow["Email"].ToString();
            string address = TestContext.DataRow["CreditAddress"].ToString();
            string city = TestContext.DataRow["CreditCity"].ToString();
            string state = TestContext.DataRow["CreditState"].ToString();
            string phone = TestContext.DataRow["Phone"].ToString();
            #endregion            

            #region TestDataLog

                TestContext.WriteLog("URL - " + Config.appUrl);
                TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration

            #endregion

            #region prerequisite
            try
            {
                status = CommonFunctions.GetEnvironmentURL();

                status = ProductDetailsPage.navigateToPDPPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //try
                //{
                //    Config.driver.Manage().Cookies.DeleteAllCookies();
                //    Config.driver.Navigate().Refresh();
                //}
                //catch (Exception ex)
                //{
                //    Assert.Fail(ex.Message);
                //}

                status = ProductDetailsPage.addToCartPDP(zipCode);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.clickPDPCheckOut();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = CartPage.proceedToCheckout_CartPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = LoginPage.clickGuestCheckout();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                #endregion

                #region Teststeps
                // Adding Shipping Address details
                status = ShippingPage.addShippingAddress(firstName, lastName, address, city, state, phone, zipCode, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                // Adding Billing Address details - Using the Shipping Address
                status = BillingPage.addBillingAddress(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForElement(By.ClassName("radio-option"));

            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }



            #endregion
        }

        [TestMethod]
        [TestCategory("Add To Cart Popup")]
        [Priority(1)]
        [Description("Verify Check price and all labels in Add to Cart Popup")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\AddToCartPopupBVT.csv", "AddToCartPopupBVT#csv", DataAccessMethod.Sequential)]
        public void VerifyCheckPriceAndAllLabels()
        {
            
                #region TestData
                string Zip = TestContext.DataRow["CreditZipCode"].ToString();
                #endregion

                #region TestDataLog

                TestContext.WriteLog("URL - " + Config.appUrl);
                TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration

            #endregion

            #region TestSteps
            try
            {
                status = CommonFunctions.GetEnvironmentURL();
                //1.Verify navigate to CartPage URL
                status = ProductDetailsPage.navigateToPDPPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //2.Click on CheckPrice and enter ZipCode
                status = ProductDetailsPage.addToCartPDP(Zip);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //3.Verify label for Price, Shipping method and ATP message
                Archive.WaitForElement(By.XPath("//a[(.='checkout') or (.='CHECKOUT') or (.='CheckOut') or (.='Checkout')]"));
                Assert.AreEqual(true, ProductDetailsPage.lbl_CartPageShippingmMthod.Displayed, "Shipping methosd is not displayed");
                Assert.AreEqual(true, ProductDetailsPage.lbl_CartPagePrice.Displayed, "Price is not displayed");
                Assert.AreEqual(true, ProductDetailsPage.lbl_ATPMessage.Displayed, "ATP Message is not displayed");

            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("Manage Account")]
        [Priority(1)]
        [Description("Verify My Account and Manage Wish List")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\ManageAccountBVT.csv", "ManageAccountBVT#csv", DataAccessMethod.Sequential)]
        public void ManageAcccont()
        {
            
                #region TestData
                string Zip = TestContext.DataRow["CreditZipCode"].ToString();
                string userName = TestContext.DataRow["UserName"].ToString();
                string password = TestContext.DataRow["Password"].ToString();
                string WishListName1 = TestContext.DataRow["WishListName1"].ToString();
                string EditName = TestContext.DataRow["EditName"].ToString();
                string ListName1 = TestContext.DataRow["ListName1"].ToString();
                string ListName2 = TestContext.DataRow["ListName2"].ToString();
                #endregion

                #region TestDataLog

                TestContext.WriteLog("URL - " + Config.appUrl);
                TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration

            #endregion

            #region TestSteps
            try
            {
                status = CommonFunctions.GetEnvironmentURL();
                //1. Navigate to Login page
                status = LoginPage.navigateToLoginPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //2. Enter Email Password and Click Sign In
                status = LoginPage.signIn(userName, password);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                string login_Title = LoginPage.txt_headers.Text;
                if (login_Title != "My Account")
                    Assert.AreEqual("My Account", login_Title, "The Login Title is Wrong");
                TestContext.WriteLog("My Account title validation is Successful");

                //3. Click Wishlist under left side tab
                status = WelcomePage.clickWishListUnderManageAccount("Wish Lists");
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //4.Delete  wishlist if already been created
                status = WelcomePage.deleteWishLIst();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //5. Create New Wishlist
                status = WelcomePage.createNewWishList(WishListName1);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //6 .Select already created wishlist under manage account and move to wishlist edit page
                status = WelcomePage.clickWishListUnderManageAccount(WishListName1);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //7.Edit wishlist
                status = WelcomePage.listNames(ListName1, EditName);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //8.Delete wishlist
                status = WelcomePage.listNames(ListName2);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //9.Click Order History and move to Order History page
                status = WelcomePage.clickWishListUnderManageAccount("Order History");
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //10.Verify Recent Orders link
                Assert.IsTrue(OrderDetailsPage.lnk_recentOrders.Displayed);
                TestContext.WriteLog("'Recent Orders' link is showing");

            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion

        }

        [TestMethod]
        [TestCategory("Add To Wishlist")]
        [Priority(1)]
        [Description("Verify PDP Add Wishlist")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\AddToWishListBVT.csv", "AddToWishListBVT#csv", DataAccessMethod.Sequential)]
        public void AddToWishlistFromPDP()
        {
            
                #region TestData
                string Zip = TestContext.DataRow["CreditZipCode"].ToString();
                string userName = TestContext.DataRow["UserName"].ToString();
                string password = TestContext.DataRow["Password"].ToString();
                string WishListName1 = TestContext.DataRow["WishListName1"].ToString();
                #endregion

                #region TestDataLog

                TestContext.WriteLog("URL - " + Config.appUrl);
                TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration

            #endregion

            #region Pre-requisite

                status = CommonFunctions.GetEnvironmentURL();

                status = ProductDetailsPage.navigateToPDPPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                status = ProductDetailsPage.clickCheckPriceInPDP(Zip);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                status = ProductDetailsPage.addToListPDP();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                status = LoginPage.signIn(userName, password);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

            #endregion

                #region TestSteps

            try
            {
               
                status = ProductDetailsPage.addToListPDP();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                SelectElement selectElement = new SelectElement(ProductDetailsPage.slct_PDPWishList);

                selectElement.SelectByValue("create");
                Archive.WaitForPageLoad();
                Thread.Sleep(3000);

                //18. Enter Name and Click on Create Button
                ProductDetailsPage.txt_createWishList.SendKeys(WishListName1);
                ProductDetailsPage.btn_createButton.Click();
                Archive.WaitForPageLoad();
                Thread.Sleep(3000);
                TestContext.WriteLog("Wish List '" + WishListName1 + "' created");

            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("PDP Page")]
        [Priority(1)]
        [Description("PDP Page - Navigation through product search")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\PDPPageBVT.csv", "PDPPageBVT#csv", DataAccessMethod.Sequential)]

        // [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "111858", DataAccessMethod.Sequential)]

        public void PDPPageNavigationThroughProductSearch()
        {

            #region TestData
            string ZipCode = TestContext.DataRow["CreditZipCode"].ToString();
            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration

            #endregion

            #region TestSteps

            try
            {
                status = CommonFunctions.GetEnvironmentURL();
                //1. Navigate to SearchResultsPage
                status = SearchResultsPage.navigateToSearchResultsPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = SearchResultsPage.selectProductFromSearchResultsPage(false, true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.clickCheckPriceInPDP(ZipCode);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.PDPShippingMethodValidation();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);
            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion

        }


        [TestMethod]
        [TestCategory("PDP Page")]
        [Priority(1)]
        [Description("PDP Page - navigation through catgory and subcategory pathway")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\PDPPageBVT.csv", "PDPPageBVT#csv", DataAccessMethod.Sequential)]
        public void PDPPageNavigationThroughCategorySubcategoryPathway()
        {
            #region TestData
            string ZipCode = TestContext.DataRow["CreditZipCode"].ToString();

            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration

            #endregion

            #region TestSteps
            try
            {
                status = CommonFunctions.GetEnvironmentURL();
                //1. Navigate to SubCategoryPage
                status = SearchResultsPage.navigateToSubCategoryPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = SearchResultsPage.selectProductFromSearchResultsPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.clickCheckPriceInPDP(ZipCode);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.PDPShippingMethodValidation();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);
            }
            catch (Exception ex)
            {
                string strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }

            #endregion
        }


        [TestMethod]
        [TestCategory("PDP Page")]
        [Priority(1)]
        [Description("PDP product validation")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\PDPPageBVT.csv", "PDPPageBVT#csv", DataAccessMethod.Sequential)]
        public void PDPProductValidation()
        {

            #region TestData
            string ZipCode = TestContext.DataRow["CreditZipCode"].ToString();

            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration

            #endregion//C:\Users\Vbabu\Source\Repos\sitecore_buildverificationtests\SiteCore_Regression_Automation\Pages\PayPalPage.cs

            #region TestSteps

            try
            {
                status = CommonFunctions.GetEnvironmentURL();
                //1. Navigate to HomePage
                status = ProductDetailsPage.navigateToPDPPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                status = ProductDetailsPage.clickCheckPriceInPDP(ZipCode);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //2. Shipping method validation in PDP page
                status = ProductDetailsPage.PDPShippingMethodValidation();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //3. Product description validation in PDP page
                status = ProductDetailsPage.verifyPDPProductDescription();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);

                //4. Product dimension validation in PDP page
                status = ProductDetailsPage.verifyPDPProductDimension();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"), status);
            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }
            #endregion
        }


        [TestMethod]
        [TestCategory("Confirm Order Page")]
        [Priority(1)]
        [Description("Confirm your order page validation")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\ConfirmYourOrderPageValidationBVT.csv", "ConfirmYourOrderPageValidationBVT#csv", DataAccessMethod.Sequential)]
        public void ConfirmYourOrderPageValidation()
        {
            #region Testdata
            string firstName = TestContext.DataRow["FirstName"].ToString();
            string lastName = TestContext.DataRow["LastName"].ToString();
            Random randomGenerator = new Random();
            double randomInt = randomGenerator.NextDouble();
            string email = "venhatesh" + randomInt + "@abc.com";
            string address = TestContext.DataRow["CreditAddress"].ToString();
            string city = TestContext.DataRow["CreditCity"].ToString();
            string state = TestContext.DataRow["CreditState"].ToString();
            string phone = TestContext.DataRow["Phone"].ToString();
            string username = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            string creditCardNumber = TestContext.DataRow["CreditCardNumber"].ToString();
            string securityCode = TestContext.DataRow["SecurityCode"].ToString();
            string expMonth = TestContext.DataRow["expMonth"].ToString();
            string expYear = TestContext.DataRow["expYear"].ToString();
            string zipCode = TestContext.DataRow["CreditZipCode"].ToString();
            string editMailAddress = TestContext.DataRow["editMailAddress"].ToString();
            string ashleyCardNumber = TestContext.DataRow["AshleyCardNumber"].ToString();
            string financingOption = TestContext.DataRow["FinancingOption"].ToString();
            string productCount = TestContext.DataRow["productCount"].ToString();
            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.appUrl);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration

            #endregion

            #region prerequisite
            try
            {
                status = CommonFunctions.GetEnvironmentURL();

                status = ProductDetailsPage.navigateToPDPPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                status = ProductDetailsPage.addToCartPDP(zipCode);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                status = ProductDetailsPage.clickPDPCheckOut();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                status = CartPage.proceedToCheckout_CartPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                status = LoginPage.clickGuestCheckout();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);
              
                status = ShippingPage.addShippingAddress(firstName, lastName, address, city, state, phone, zipCode, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                status = BillingPage.addBillingAddress(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                status = PaymentPage.enterAshleyAdvantageCardPayments(ashleyCardNumber, financingOption);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                #endregion

                #region Teststeps

                //1.editing shipping address details
                status = ConfirmOrderPage.editShippingDetails(editMailAddress);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //2.Verify edited shipping address details
                status = ConfirmOrderPage.verifyEditedShippingDetails(editMailAddress);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //3.Edit billing details
                status = ConfirmOrderPage.editBillingDetails(editMailAddress);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //4.Verify edited billing details
                status = ConfirmOrderPage.verifyEditedBillingDetails(editMailAddress);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //5.Edit and verify payment type in confirm order page
                status = ConfirmOrderPage.editAndVerifyPaymentTypeForAshleyCard(firstName, lastName, creditCardNumber, securityCode, expMonth, expYear);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //6.Edit product quantity details
                status = ConfirmOrderPage.editProductQuantityDetail(productCount);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //7.Verify product quantity details
                status = ConfirmOrderPage.verifyUpdatedQuantityDetails(productCount);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

                //8.Removal of product from confirm order page
                status = ConfirmOrderPage.removeProductFromConfirmOrderPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"),status);

            }
            catch (Exception ex)
            {
                String strFinal =  TestContext.TestName + ".PNG";
                Archive.TakeScreenshot(strFinal);
                Assert.Fail(ex.Message);
            }

            #endregion
        }

       

        [TestInitialize]
        public void TestStartup()
        {
            TestContext.Properties.Add("AppUrl", CommonFunctions.GetEnvironmentURL());
            Archive.SetConfig(TestContext);            
            Archive.LaunchDriver(false);            
            IWebDriver a = Config.driver;
            CommonFunctions = new CommonFunctions();
            BillingPage = new BillingPage();
            CartPage = new CartPage();
            ConfirmOrderPage = new ConfirmOrderPage();
            Footer = new Footer();
            Header = new Header();
            HomePage = new HomePage();
            LoginPage = new LoginPage();
            OrderDetailsPage = new OrderDetailsPage();
            PaymentPage = new PaymentPage();
            PayPalPage = new PayPalPage();
            ProductDetailsPage = new ProductDetailsPage();
            SearchResultsPage = new SearchResultsPage();
            ShippingPage = new ShippingPage();
            StoreLocatorPage = new StoreLocatorPage();
            ThankYouPage = new ThankYouPage();
            WelcomePage = new WelcomePage();
            //TestContext.Properties.Remove("AppUrl");
            

            if (Config.browserName == "InternetExplorer")
            {
                List<Cookie> lstContent = Config.driver.Manage().Cookies.AllCookies.ToList();

                Config.driver.Manage().Cookies.DeleteAllCookies();
                List<Cookie> lstContent1 = Config.driver.Manage().Cookies.AllCookies.ToList();
                Thread.Sleep(10000);
                Config.driver.Navigate().Refresh();
                List<Cookie> lstContent2 = Config.driver.Manage().Cookies.AllCookies.ToList();
            } 

            

        }

        [TestCleanup]
        public void TestCleanup()
        {
            Config.driver.Quit();
        }


        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
