﻿using AshleyAutomationLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class Footer
    {
        public Footer()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        [FindsBy(How = How.ClassName, Using = "row toutBlock short")]
        public IWebElement elm_contactUsHelp;

        [FindsBy(How = How.Id, Using = "general-topic")]
        public IWebElement dd_subjectDropDownParent;

        [FindsBy(How = How.Id, Using = "general-customer-name")]
        public IWebElement txt_contactUsOption5Name;

        [FindsBy(How = How.Id, Using = "general-phone-number")]
        public IWebElement txt_contactUsOption5PhoneNumber;

        [FindsBy(How = How.Id, Using = "general-email")]
        public IWebElement txt_contactUsOption5Email;

        [FindsBy(How = How.Id, Using = "general-comments")]
        public IWebElement txt_contactUsOption5Comments;

        [FindsBy(How = How.Id, Using = "contact-confirmation")]
        public IWebElement elm_contactUsOption5ConfirmationPopUp;

        [FindsBy(How = How.Id, Using = "product-inquire-furniture-type")]
        public IWebElement txt_contactUsHelpOption1ProductType;

        [FindsBy(How = How.Id, Using = "product-inquire-customer-name")]
        public IWebElement txt_contactUsHelpOption1FullName;

        [FindsBy(How = How.Id, Using = "product-inquire-phone-number")]
        public IWebElement txt_contactUsHelpOption1PhoneNumber;

        [FindsBy(How = How.Id, Using = "product-inquire-email")]
        public IWebElement txt_contactUsHelpOption1Email;

        [FindsBy(How = How.Id, Using = "product-inquire-comments")]
        public IWebElement txt_contactUsHelpOption1Comments;

    }
}
