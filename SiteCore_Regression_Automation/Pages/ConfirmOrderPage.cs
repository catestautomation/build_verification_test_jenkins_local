﻿using AshleyAutomationLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class ConfirmOrderPage
    {
        public ConfirmOrderPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        public static string result = string.Empty;

        //public static object img_ashleyLogo = new { Class = "afhs-cart-logo" };
        [FindsBy(How = How.ClassName, Using = "afhs-cart-logo")]
        public  IWebElement img_ashleyLogo;

        //public static object elm_shippingAddress = new { Class = "msax-ShippingAddressData" };
        [FindsBy(How = How.ClassName, Using = "msax-ShippingAddressData")]
        public IWebElement elm_shippingAddress;

        //public static object elm_billingAddress = new { Class = "msax-BillingAddressData" };
        [FindsBy(How = How.ClassName, Using = "msax-BillingAddressData")]
        public  IWebElement elm_billingAddress;

        //public static object elm_shippingMethod = new { Class = "msax-ShippingMethod" };
        [FindsBy(How = How.ClassName, Using = "msax-ShippingMethod")]
        public  IWebElement elm_shippingMethod;

        
        [FindsBy(How = How.ClassName, Using = "msax-OrderSummary")]
        public IWebElement elm_orderSummary;

      
        [FindsBy(How = How.ClassName, Using = "msax-ProductName")]
        public IWebElement elm_productName;

        [FindsBy(How = How.ClassName, Using = "msax-QuantityContent")]
        public IWebElement elm_quantity;

        [FindsBy(How = How.ClassName, Using = "msax-LineTotal")]
        public IWebElement elm_total;
 
        [FindsBy(How = How.Id, Using = "scphbody_0_OrderDetailEdit")]
        public IWebElement lnk_editAddress;

        [FindsBy(How = How.Id, Using = "LblProductAdded")]
        public IWebElement elm_labelProduct;

        [FindsBy(How = How.Id, Using = "WishListContinueShopping")]
        public IWebElement btn_continueShoppingButton;

        [FindsBy(How = How.Id, Using = "scphbody_0_termsSubmitWrapper")]
        public IWebElement btn_termsOfSubmitOrderButton;

        [FindsBy(How = How.Id, Using = "termsOfUseLightbox")]
        public IWebElement con_termsOfUseLightBox;

        [FindsBy(How = How.Id, Using = "termsAndConditionsLightbox")]
        public static IWebElement con_termsAndConditionsLightBox;

        [FindsBy(How = How.Id, Using = "privacyPolicyLightbox")]
        public IWebElement con_privacyPolicyLightbox;

        [FindsBy(How = How.Id, Using = "stateRecyclingFee")]
        public IWebElement con_recyclingFeePopUp;

        [FindsBy(How = How.Id, Using = "rsaIndicatorBlock")]
        public IWebElement con_rsaIndicatorBlock;

        [FindsBy(How = How.Id, Using = "showRsaIndicatorBlock")]
        public IWebElement con_editRsaIndicatorBlock;

        [FindsBy(How = How.Id, Using = "navigateAwayModal")]
        public IWebElement con_leavePopUp;

        [FindsByAll]
        [FindsBy(How = How.XPath, Using = "//span[contains(.,'(Edit)')]")]
        public IList<IWebElement> lnk_Address;

        [FindsBy(How = How.Id, Using = "toPopup1")]
        public  IWebElement con_promocodePopUp;

        [FindsBy(How = How.ClassName, Using = "close")]
        public IWebElement btn_closeLightBox;

        [FindsBy(How = How.XPath, Using = "//button[.='Submit order']")]
        public IWebElement btn_submitOrder;

        [FindsBy(How = How.ClassName, Using = "msax-PayPalLogo")]
        public IWebElement img_paypalLogo;

        [FindsBy(How = How.XPath, Using = "//button[.='Place Order']")]
        public IWebElement btn_PlaceOrder;

        [FindsByAll]
        [FindsBy(How = How.Id, Using = "txtbxQuantityIncrementalID")]
        public IList<IWebElement> txt_quantity;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-ProductName")]
        public IList<IWebElement> lnk_productName;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-ProductId")]
        public  IList<IWebElement> elm_productId;

        [FindsBy(How = How.ClassName, Using = "msax-ItemsGrid")]
        public IWebElement elm_confirmationNumber;

        [FindsByAll]
        [FindsBy(How = How.XPath, Using = "//a[contains(@class,'ui-state-default')]")]
        public IList<IWebElement> ele_selectDate;

        [FindsBy(How = How.Id, Using = "scphbody_0_ctl04_btnContinue")]
        public IWebElement btn_scheduledContinue;

        [FindsBy(How = How.ClassName, Using = "msax-DateSelected")]
        public IWebElement elm_selectedDate;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-Email")]
        public IList<IWebElement> elm_AddressEmail;

        [FindsBy(How = How.Id, Using = "txtbxQuantityIncrementalID")]
        public IWebElement txt_QuantityEdit;

        [FindsBy(How = How.XPath, Using = "//span[@class='msax-Quantity']")]
        public IList<IWebElement> ele_ProductQuantity;

        [FindsBy(How = How.XPath, Using = "//button[.='Update']")]
        public IList<IWebElement> ele_ProductCountUpdate;

        [FindsBy(How = How.XPath, Using = "//button[.='Remove']")]
        public IList<IWebElement> lnk_ProductRemove;

        [FindsBy(How = How.ClassName, Using = "msax-Image")]
        public IList<IWebElement> img_ProductCount;

        [FindsBy(How = How.ClassName, Using = "msax-Last4Digits")]
        public IWebElement ele_PaymentCardLast4Digits;



        By editAddressLink = By.XPath("//span[contains(.,'(Edit)')]");

        By lnk_update = By.XPath("//button[.='Update']");

        By lnk_remove = By.XPath("//button[.='Remove']");

        By img_Product = By.ClassName("msax-Image");


        public string placeOrder()
        {
            try
            {
                Archive.WaitForElement(By.XPath("//button[.='Place Order']"));
                new Actions(Config.driver).MoveToElement(btn_PlaceOrder).Click().Perform();
                Archive.WaitForPageLoad();
                result = "'Place Order' button is clicked successfully";
            }
            catch (Exception ex)
            {
                result = "Failure: 'Place Order' button is not clicked successfully" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        public string submitOrder()
        {
            try
            {
                Archive.WaitForElement(By.XPath("//div[@class='msax-Loading']"),false);
                Archive.WaitForElement(By.XPath("//button[.='Submit order']"));
                //Thread.Sleep(25000);
                new Actions(Config.driver).MoveToElement(btn_submitOrder).Click().Perform();
                Archive.WaitForPageLoad();
                result = "'Submit Order' button is not clicked";
            }
            catch (Exception ex)
            {
                result = "Failure: 'Submit Order' button is not clicked" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// BVT-Shipping address edit
        /// </summary>
        /// <param name="mailAddress"></param>
        /// <returns></returns>
      
        public string editShippingDetails(String mailAddress)
        {
            ShippingPage ShippingPage = new ShippingPage();

            try
            {
                Archive.WaitForPageLoad();
                var wait = new WebDriverWait(Config.driver, TimeSpan.FromMinutes(3));


                Archive.WaitForElement(editAddressLink);
                lnk_Address[0].Click();

                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));

                ShippingPage.ele_txt_email.Clear();
                ShippingPage.ele_txt_email.SendKeys(mailAddress);

                ShippingPage.ele_Txt_confirmEmail.Clear();
                ShippingPage.ele_Txt_confirmEmail.SendKeys(mailAddress);

                ShippingPage.ele_Txt_confirmEmail.SendKeys(Keys.Tab);

                ShippingPage.ele_btn_continue.Click();

                Archive.WaitForPageLoad();
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));

                result = "Shipping details are edited successfully";

            }
            catch (Exception ex)
            {
                result = "Failure while editing shipping details" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// BVT-editing Billing address
        /// </summary>
        /// <param name="mailAddress"></param>
        /// <returns></returns>
        public string editBillingDetails(String mailAddress)
        {
            BillingPage BillingPage = new BillingPage();

            try
            {
                var wait = new WebDriverWait(Config.driver, TimeSpan.FromMinutes(3));
                wait.PollingInterval = TimeSpan.FromMilliseconds(250);
                //wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));

                Archive.WaitForElement(editAddressLink);
                lnk_Address[1].Click();

                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));

                IWebElement ele_btnsholder = BillingPage.ele_btn_container.FindElement(By.ClassName("msax-ButtonSection"));

                List<IWebElement> ele_btn_Continue = ele_btnsholder.GetDescendants();//GetChildren was used

                IWebElement ele_Continue = ele_btn_Continue[1];

                BillingPage.ele_txt_billingEmail.Clear();
                BillingPage.ele_txt_billingEmail.SendKeys(mailAddress);

                BillingPage.ele_Txt_confirmEmail.Clear();
                BillingPage.ele_Txt_confirmEmail.SendKeys(mailAddress);

                BillingPage.ele_Txt_confirmEmail.SendKeys(Keys.Tab);

                ele_Continue.Click();

                Archive.WaitForPageLoad();

                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));
                result = "Billing details are edited successfully";

            }
            catch (Exception ex)
            {
                result = "Failure while editing Billing details page" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// BVT-Payment type edit and its validation in confirm order page
        /// </summary>
        /// <param name="ashleyCardNumber"></param>
        /// <param name="financingOption"></param>
        /// <returns></returns>
        public string editAndVerifyPaymentTypeForAshleyCard(string firstName, string lastName, string creditCardNumber, string securityCode, string expMonth, string expYear)
        {
            PaymentPage PaymentPage = new PaymentPage();
            try
            {
                var wait = new WebDriverWait(Config.driver, TimeSpan.FromMinutes(3));
                wait.PollingInterval = TimeSpan.FromMilliseconds(250);
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));
                string cardNoBeforeEdit = ele_PaymentCardLast4Digits.Text;
                Archive.WaitForElement(editAddressLink);
                lnk_Address[2].Click();
                Archive.WaitForPageLoad();
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));
                PaymentPage.enterCreditPayments(firstName, lastName, creditCardNumber, securityCode, expMonth, expYear);
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));
                if (!(ele_PaymentCardLast4Digits.Text.Equals(cardNoBeforeEdit)))
                {
                    result = "payment type edit is successful";
                }
                else
                {
                    throw new Exception("Payment type edit process is failed");
                }

            }
            catch (Exception ex)
            {
                result = "Failure" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// BVT- Verify edited billing details
        /// </summary>
        /// <param name="mailAddress"></param>
        /// <returns></returns>
        public string verifyEditedBillingDetails(String mailAddress)
        {
            var wait = new WebDriverWait(Config.driver, TimeSpan.FromMinutes(3));
            wait.PollingInterval = TimeSpan.FromMilliseconds(250);
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));
            Thread.Sleep(3000);
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));
            try
            {
                if (elm_AddressEmail[1].Text.ToLower() == mailAddress)
                {
                    result = "edited billing details is reflected in confirm order page";
                }
                else
                {
                    throw new Exception("Failure: edited billing address is not reflected in confirmation page");
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        /// <summary>
        /// BVT-Verify edited shipping details
        /// </summary>
        /// <param name="mailAddress"></param>
        /// <returns></returns>
        public string verifyEditedShippingDetails(String mailAddress)
        {
            var wait = new WebDriverWait(Config.driver, TimeSpan.FromMinutes(3));
            wait.PollingInterval = TimeSpan.FromMilliseconds(250);
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));
            Thread.Sleep(8000);
            try
            {
                if (elm_AddressEmail[0].Text.ToLower() == mailAddress)
                {
                    result = "edited shipping details is reflected in confirm order page";
                }
                else
                {
                    throw new Exception("edited shipping address is not reflected in confirmation page");
                }
            }
            catch (Exception ex)
            {
                result = "Failure:" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// BVT-edit product quantity details
        /// </summary>
        /// <param name="productCount"></param>
        /// <returns></returns>
        public string editProductQuantityDetail(string productCount)
        {
            try
            {
                txt_QuantityEdit.Clear();
                txt_QuantityEdit.SendKeys(productCount);
                Archive.WaitForElement(lnk_update);
                ele_ProductCountUpdate[0].Click();
                var wait = new WebDriverWait(Config.driver, TimeSpan.FromMinutes(3));
                wait.PollingInterval = TimeSpan.FromMilliseconds(250);
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));
                result = "Product quantity is edited successful with" + productCount;
            }
            catch (Exception ex)
            {
                result = "Failure in product quantity edit";
            }
            return result;
        }

        /// <summary>
        /// BVT-Updated quantity details verification in ConfirmOrder page
        /// </summary>
        /// <param name="productCount"></param>
        /// <returns></returns>
        public string verifyUpdatedQuantityDetails(string productCount)
        {
            try
            {
                if (ele_ProductQuantity[0].Text.Equals(productCount))
                {
                    result = "Updated product count verification is successful";
                }
                else
                {
                    throw new Exception("Failure in Updated product count verification");
                }
                String stra = ele_ProductQuantity[0].Text;
            }
            catch (Exception ex)
            {
                result = ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// BVT- Removal and verification of removed product in Confirm order page
        /// </summary>
        /// <returns></returns>
        public string removeProductFromConfirmOrderPage()
        {
            try
            {
                Archive.WaitForElement(lnk_remove);
                Archive.WaitForElement(img_Product);
                int productCountBeforeReomoval = img_ProductCount.Count;
                lnk_ProductRemove[0].Click();
                var wait = new WebDriverWait(Config.driver, TimeSpan.FromMinutes(3));
                wait.PollingInterval = TimeSpan.FromMilliseconds(250);
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));
                if (img_ProductCount.Count.Equals(productCountBeforeReomoval - 1))
                {
                    result = "Product removed from confirm order page";
                }
                else
                {
                    throw new Exception("Failure in product removal process");
                }

            }
            catch (Exception ex)
            {
                result = "Failure" + ex.Message + ex.StackTrace;
            }
            return result;
        }

    }
}
