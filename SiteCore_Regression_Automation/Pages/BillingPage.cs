﻿using OpenQA.Selenium;
using AshleyAutomationLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace SiteCore_Regression_Automation.Pages
{
    public class BillingPage
    {
        public static string result = string.Empty;

        public BillingPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        [FindsBy(How = How.Id, Using = "UseShippingAddressCheckBox")]
        public IWebElement ele_chk_useShippingAddress;

        [FindsBy(How = How.Id, Using = "stateRecyclingFee")]
        public IWebElement ele_con_recyclingFeePopUp;

        [FindsBy(How = How.Id, Using = "PaymentAddressFirstName")]
        public IWebElement ele_txt_billingFirstName;

        [FindsBy(How = How.Id, Using = "PaymentAddressLastName")]
        public IWebElement ele_txt_billingLastName;

        [FindsBy(How = How.Id, Using = "PaymentAddressStreet1")]
        public IWebElement ele_txt_billingAddressLine1;

        [FindsBy(How = How.Id, Using = "PaymentAddressCity")]
        public IWebElement ele_txt_billingCity;

        [FindsBy(How = How.Id, Using = "PaymentAddressState")]
        public IWebElement ele_dd_billingState;

        [FindsBy(How = How.Id, Using = "OrderAddressZipCode")]
        public IWebElement ele_txt_billingZipCode;

        [FindsBy(How = How.Id, Using = "PaymentPhoneNumber1")]
        public IWebElement ele_txt_billingPhone;

        [FindsBy(How = How.Id, Using = "EmailTextBox")]
        public IWebElement ele_txt_billingEmail;

        [FindsBy(How = How.Id, Using = "ConfirmBillingEmailTextBox")]
        public IWebElement ele_Txt_confirmEmail;

        [FindsBy(How = How.LinkText, Using = "CONTINUE")]
        public IWebElement ele_btn_continue;

        [FindsBy(How = How.ClassName, Using = "msax-BillingAddress")]
        public IWebElement ele_btn_container;

        /// <summary>
        /// addBillingAddress() function is used to enter the Billing Address details of the user
        /// </summary>
        /// <param name="useThisAddress"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="address"></param>
        /// <param name="city"></param>
        /// <param name="stateCode"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="zipCode"></param>
        /// <param name="email"></param>
        /// <returns></returns>

        public string addBillingAddress(bool useThisAddress = true, String firstName = "", String lastName = "", String address = "",
             String city = "", String stateCode = "", String phoneNumber = "", String zipCode = "", String email = "")
        {
            try
            {
                //Thread.Sleep(10000);
                Archive.WaitForElement(By.XPath("//div[@class='msax-Loading']"),false);
                Archive.WaitForElement(By.Id("UseShippingAddressCheckBox"));
                IWebElement ele_btnsholder = ele_btn_container.FindElement(By.ClassName("msax-ButtonSection"));

                List<IWebElement> ele_btn_Continue = ele_btnsholder.GetDescendants();//GetChildren was used

                IWebElement ele_Continue = ele_btn_Continue[1];

                if (useThisAddress)
                {
                    ele_chk_useShippingAddress.Click();
                    Archive.WaitForPageLoad();
                    ele_Continue.Click();
                    Archive.WaitForPageLoad();
                }
                else
                {

                    ele_txt_billingFirstName.SendKeys(firstName);

                    ele_txt_billingLastName.SendKeys(lastName);

                    ele_txt_billingAddressLine1.SendKeys(address);

                    ele_txt_billingCity.SendKeys(city);

                    var selectElement = new SelectElement(ele_dd_billingState);
                    selectElement.SelectByText(stateCode);

                    ele_txt_billingZipCode.SendKeys(zipCode);

                    ele_txt_billingPhone.SendKeys(phoneNumber);

                    ele_txt_billingEmail.SendKeys(email);

                    ele_Txt_confirmEmail.SendKeys(email);

                    ele_Continue.Click();
                    Archive.WaitForPageLoad();

                    //Thread.Sleep(7000);
                    
                }
                result = "Adding billing details are performed succesfully";
            }
            catch (Exception ex)
            {
                result = "Failure in adding billing details" + ex.Message + ex.StackTrace;
            }

            return result;

        }

    }

}
