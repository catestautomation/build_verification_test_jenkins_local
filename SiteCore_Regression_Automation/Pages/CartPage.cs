﻿using AshleyAutomationLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using SiteCore_Regression_Automation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public  class CartPage
    {

        private  string strReplaceContent;
        public string result = string.Empty;

        public CartPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        [FindsBy(How = How.ClassName, Using = "view-cart-content")]
        public  IWebElement con_cartContent;

        [FindsByAll]
        [FindsBy(How = How.XPath, Using = "//button[.='Update']")]
        public IList<IWebElement> btn_update;

        [FindsByAll]
        [FindsBy(How = How.XPath, Using = "//button[.='Remove']")]
        public IList<IWebElement> btn_remove;

        [FindsByAll]
        [FindsBy(How = How.Id, Using = "txtbxQuantityIncrementalID")]
        public IList<IWebElement> txt_quantity;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-LineTotal")]
        public IList<IWebElement> txt_subTotal;

        [FindsBy(How = How.Id, Using = "toPopup1")]
        public  IWebElement con_promoCodePopUp;

        [FindsBy(How = How.ClassName, Using = "msax-Promocodeitem")]
        public  IWebElement mge_promoCodeValid;

        [FindsBy(How = How.Id, Using = "lblError1")]
        public  IWebElement lbl_errorMge;

        [FindsBy(How = How.Id, Using = "stateRecyclingFee")]
        public  IWebElement con_recyclingFeePopUp;

        [FindsBy(How = How.ClassName, Using = "msax-ContinueShoppingButton")]
        public  IWebElement btn_continueShoppingAtCart;

        [FindsBy(How = How.ClassName, Using = "close")]
        public  IWebElement btn_closeLightBox;

        [FindsBy(How = How.XPath, Using = "//span[.='Click to Enter Promo Code(s)']")]
        public  IWebElement btn_enterPromoCodeLightBox;
       
        [FindsBy(How = How.ClassName, Using = "msax-PromotionCodeTextBox")]
        public IWebElement txt_promoCodeField;

        [FindsBy(How = How.ClassName, Using = "msax-ApplyPromotionCode")]
        public IWebElement btn_applyPromoCodeButton;

        [FindsBy(How = How.XPath, Using = "//span[.='Edit Promo Code(s)']")]
        public IWebElement edit_PromoCode;

        [FindsBy(How = How.XPath, Using = "//button[.='(Remove)']")]
        public IWebElement Promo_Click_Remove;

        [FindsBy(How = How.Id, Using = "rsaIndicatorView")]
        public  IWebElement con_rsaIndicatorOption;

        [FindsBy(How = How.Id, Using = "showRsaIndicatorBlock")]
        public  IWebElement lnk_editRsaIndicatorBlock;

        [FindsBy(How = How.Id, Using = "addToWishList")]
        public  IWebElement con_addToWishListPopUp;

        [FindsBy(How = How.Id, Using = "newWishListName")]
        public  IWebElement txt_newWishListName;

        [FindsBy(How = How.Id, Using = "createWhishListSuccess")]
        public  IWebElement con_createWishListSuccessPopUp;

        [FindsBy(How = How.Id, Using = "WishListContinueShopping")]
        public  IWebElement btn_continueShoppingButton;

        [FindsByAll]
        [FindsBy(How = How.PartialLinkText, Using = "Checkout",Priority = 0)]
        [FindsBy(How = How.TagName, Using = "button", Priority = 1)]
        public  IWebElement btn_NewCheckOutCartPage;


        [FindsBy(How = How.XPath, Using = ("/descendant::span[contains(.,'Proceed to Checkout')]"))]
        public  IWebElement btn_checkOutCartPage;     

        [FindsBy(How = How.ClassName, Using = "search-box__input")]
        public  IWebElement txt_cartPageSearchInputField;
        
        [FindsBy(How = How.ClassName, Using = "search-box__icon")]
        public  IWebElement btn_CartPageSearchInputbutton;

        //public  object btn_CartPageSearchInputbutton = new { Id = "searchIcon" };//Ashcomm5//Ashcomm6
        //public  object lbl_shippingCharge = new {  Class = "msax-EstHomeDelivery" };
        [FindsBy(How = How.ClassName, Using = "msax-EstHomeDelivery")]
        public  IWebElement lbl_shippingCharge;

        [FindsBy(How = How.ClassName, Using = "msax-ContinueShoppingButton")]
        public  IWebElement lnk_continueShoppingAtCartPage;
        
        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "product-summary__list", Priority = 0)]
        [FindsBy(How = How.TagName, Using = "ul", Priority = 1)]
        public  IWebElement productSummaryListAtCartPage;

 
        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-ProductId", Priority = 0)]
        public IList<IWebElement> lbl_skuNumberAtCartPage;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-ProductName", Priority = 0)]
        public IList<IWebElement> lbl_productNameAtCartPage;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-ImageWrapper", Priority = 0)]
        public IList<IWebElement> lbl_productImageAtCartPage;

        [FindsByAll]
        [FindsBy(How = How.XPath, Using = "//*[.='Qty:']")]
        public IList<IWebElement> lbl_productQuantityAtCartPage;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-SoldBy", Priority = 0)]
        public IList<IWebElement> lbl_fulfilledByAtCartPage;


        [FindsBy(How = How.PartialLinkText, Using = "CHECKOUT")]
        public  IWebElement btn_CHECKOUT;

        [FindsBy(How = How.PartialLinkText, Using = "Checkout")]
        public  IWebElement btn_Checkout;

        [FindsBy(How = How.PartialLinkText, Using = "CHECKOUT")]
        public  IWebElement btn_CONTINUESHOPPING;

        [FindsBy(How = How.PartialLinkText, Using = "Continue Shopping")]
        public  IWebElement btn_ContinueShopping;

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'paypal-button')]")]
        public IWebElement btn_PaypalCheckout;

        [FindsBy(How = How.Id, Using = "rsaIndicatorTitle")]
        public IWebElement RSA_Field;

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'applyRsaIndicatorBtn')]")]
        public IWebElement RSA_btn;

        [FindsBy(How = How.Id, Using = "rsaIndicatorTextBox")]
        public IWebElement RSA_Field_Txt;

        [FindsBy(How = How.XPath, Using = "//button[.='(edit)']")]
        public IWebElement RSA_Click_Edit;

        [FindsBy(How = How.XPath, Using = "//button[.='(remove)']")]
        public IWebElement RSA_Click_Remove;

        [FindsBy(How = How.Id, Using = "UpdateZipcodeBtn")]
        public IWebElement updatezipCode;

        [FindsBy(How = How.Id, Using = "zipCodeTextbox")]
        public IWebElement txt_zipCode;

        [FindsBy(How = How.XPath, Using = "//span[.='You have changed the delivery zip-code, price(s) in your cart may have changed.']")]
        public IWebElement zipCodeChangeErrorMsg;

        [FindsByAll]
        [FindsBy(How = How.XPath, Using = "//button[.='Move to Wish List']")]
        public IList<IWebElement> btn_moveToWishList;



        By RSA_Apply_btn = By.XPath("//button[contains(@class,'applyRsaIndicatorBtn')]");
        By RSA_input_Field = By.Id("rsaIndicatorTextBox");
        By RSA_Edit_Click = By.XPath("//button[.='(edit)']");
        By RSA_Remove_Click = By.XPath("//button[.='(remove)']");



        /// <summary>
        /// Express Paypal Payment details are entered and address is modified
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="passWord"></param>
        /// <returns></returns>
        public string expressPayPalPaymentEditAddress(String userName, String passWord, String firstName, String lastName, String address1, String city, String state, string zipCode)
        {
            try
            {
                PaymentPage PaymentPage = new PaymentPage();
                PayPalPage PayPalPage = new PayPalPage();

                IJavaScriptExecutor jse = (IJavaScriptExecutor)Config.driver;
                jse.ExecuteScript("window.scrollBy(0,1500)", "");
                btn_PaypalCheckout.Click();
                Archive.WaitForPageLoad();
                Thread.Sleep(10000);

                // IWebDriver driver = GlobalProperties.driver;
                string currentWindowHandle = Config.driver.CurrentWindowHandle;
                Config.driver.SwitchTo().Window(Config.driver.WindowHandles.Last());

                Config.driver.Manage().Window.Maximize();

                IWebElement iframe_object = Config.driver.FindElement(By.Name("injectedUl"));

                Config.driver.SwitchTo().Frame(iframe_object);

                Archive.WaitForElement(By.Id("email"));
                PaymentPage.btn_paypalEmail.SendKeys(userName);

                PaymentPage.btn_paypalPassword.SendKeys(passWord);

                PaymentPage.btn_paypalLogin.Click();
                Thread.Sleep(15000);

                Config.driver.SwitchTo().DefaultContent();
                PayPalPage.lnk_Change[0].Click();
                Archive.WaitForElement(By.XPath("//span[contains(@class,'leftText')]"));

                PayPalPage.lnk_Add.Click();
                Archive.WaitForElement(By.Id("firstName"));

                PayPalPage.txt_firstName.SendKeys(firstName);
                PayPalPage.txt_lastName.SendKeys(lastName);
                PayPalPage.txt_streetAddress1.SendKeys(address1);
                PayPalPage.txt_city.SendKeys(city);

                var selectElement = new SelectElement(PayPalPage.slct_state);
                selectElement.SelectByText(state);

                PayPalPage.txt_zipCode.SendKeys(zipCode);

                PayPalPage.btn_save.Click();
                Archive.WaitForPageLoad();

                Archive.WaitForElement(By.Id("confirmButtonTop"));
                PaymentPage.btn_paypalContinuePayment.Click();
                Thread.Sleep(5000);

                Config.driver.SwitchTo().Window(currentWindowHandle);
                Thread.Sleep(2000);
                result = "Entering Paypal payment details and modification of address is succesful";
            }
            catch (Exception ex)
            {
                result = "Failure in Entering paypal payment details and Modifying address" + ex.Message + ex.StackTrace;
            }

            return result;


        }

        /// <summary>
        /// Express Paypal Payment details are entered succesful
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="passWord"></param>
        /// <returns></returns>
        public string expressPayPalPaymentCheckOut(String userName, String passWord)
        {
            try
            {
                PaymentPage PaymentPage = new PaymentPage();
                PayPalPage PayPalPage = new PayPalPage();

                IJavaScriptExecutor jse = (IJavaScriptExecutor)Config.driver;
                jse.ExecuteScript("window.scrollBy(0,900)", "");
                btn_PaypalCheckout.Click();
                Archive.WaitForPageLoad();
                Thread.Sleep(10000);

                // IWebDriver driver = GlobalProperties.driver;
                string currentWindowHandle = Config.driver.CurrentWindowHandle;
                Config.driver.SwitchTo().Window(Config.driver.WindowHandles.Last());

                Config.driver.Manage().Window.Maximize();

                IWebElement iframe_object = Config.driver.FindElement(By.Name("injectedUl"));

                Config.driver.SwitchTo().Frame(iframe_object);

                Archive.WaitForElement(By.Id("email"));
                PaymentPage.btn_paypalEmail.SendKeys(userName);

                PaymentPage.btn_paypalPassword.SendKeys(passWord);

                PaymentPage.btn_paypalLogin.Click();
                Thread.Sleep(15000);

                Config.driver.SwitchTo().DefaultContent();
                
                Archive.WaitForElement(By.Id("confirmButtonTop"));
                PaymentPage.btn_paypalContinuePayment.Click();
                Thread.Sleep(5000);

                Config.driver.SwitchTo().Window(currentWindowHandle);
                Thread.Sleep(2000);
                result = "Entering Paypal payment details are succesful";
            }
            catch (Exception ex)
            {
                result = "Failure in Entering paypal payment details" + ex.Message + ex.StackTrace;
            }

            return result;


        }

        /// <summary>
   /// BVT - Enter RSA ID in Cart Page
   /// </summary>
   /// <param name="RSA_Txt"></param>
   /// <returns></returns>
        public string enterRSAID(string RSA_Txt = "")
        {
            try
            {
                Archive.WaitForElement(RSA_input_Field);
                RSA_Field_Txt.Clear();
                RSA_Field_Txt.SendKeys(RSA_Txt);
                Archive.WaitForElement(RSA_Apply_btn);
                RSA_btn.Click();
                result = "Entered RSA Code is successfully";
            }

            catch (Exception ex)
            {
                result = "Failure in entering RSA Code" + ex.Message + ex.StackTrace;
            }
            return result;
        }
        
        /// <summary>
        /// Edit RSA ID in Cart Page
        /// </summary>
        /// <param name="RSA_another_txt"></param>
        /// <returns></returns>
        public string editRSAID(string RSA_another_txt = "")
        {
            try
            {
                {
                    Archive.WaitForElement(RSA_Edit_Click);
                    RSA_Click_Edit.Click();
                    enterRSAID(RSA_another_txt);
                    result = " Clicked Edit button successfully";
                }

            }

            catch (Exception ex)
            {
                result = "Failure in Clicking Edit Button" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Remove RSA ID in Cart Page
        /// </summary>
        /// <returns></returns>
        public string removeRSAID()
        {
            try
            {
                {
                    Archive.WaitForElement(RSA_Remove_Click);
                    RSA_Click_Remove.Click();
                    result = "Clicked Removed button successfully";
                }

            }

            catch (Exception ex)
            {
                result = "Failure in Clicking Remove Button" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Update Quantity in Cart Page
        /// </summary>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public string updateQuantity(string quantity)
        {
            try
            {
                txt_quantity[0].Clear();
                txt_quantity[0].SendKeys(quantity);
                btn_update[1].Click();
                Archive.WaitForPageLoad();
                Archive.WaitForElement(By.XPath("//div[@class='msax-Loading']"), false);
                Archive.WaitForElement(By.XPath("/descendant::span[contains(.,'Proceed to Checkout')]"));                
                result = "Clicked Update button successfully";
            }
            catch (Exception ex)
            {
                result = "Failure in Clicking Update Button" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Enter PromoCode in Cart Page
        /// </summary>
        /// <param name="promoCode"></param>
        /// <returns></returns>
        public string enterPromoCode(string promoCode)
        {
            try
            {
                btn_enterPromoCodeLightBox.Click();
                Archive.WaitForElement(By.ClassName("msax-PromotionCodeTextBox"));
                txt_promoCodeField.SendKeys(promoCode);
                btn_applyPromoCodeButton.Click();
                Archive.WaitForElement(By.ClassName("msax-applyPromoCodeImg"));
                btn_closeLightBox.Click();
                result = "PromoCode applied successfully";
            }
            catch (Exception ex)
            {
                result = "Failure in applying Promocode" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Edit and Remove Promocode in Cart Page
        /// </summary>
        /// <param name="promoCode"></param>
        /// <returns></returns>
        public string editAndRemovePromoCode(string promoCode)
        {
            try
            {
                edit_PromoCode.Click();
                Archive.WaitForElement(By.ClassName("msax-applyPromoCodeImg"));
                Promo_Click_Remove.Click();
                Archive.WaitForPageLoad();
                var wait = new WebDriverWait(Config.driver, TimeSpan.FromSeconds(30));
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[@class='msax-Loading']")));              
                Archive.WaitForElement(By.ClassName("msax-PromotionCodeTextBox"));
                txt_promoCodeField.SendKeys(promoCode);
                btn_applyPromoCodeButton.Click();
                Archive.WaitForElement(By.ClassName("msax-applyPromoCodeImg"));
                btn_closeLightBox.Click();
                result = "PromoCode removed and edited successfully";
            }
            catch (Exception ex)
            {
                result = "Failure in editing and removing Promocode" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Update ZipCode in Cart Page
        /// </summary>
        /// <param name="zipCode"></param>
        /// <returns></returns>
        public string updateZipCode(string zipCode)
        {
            try
            {
                ((IJavaScriptExecutor)Config.driver).ExecuteScript("window.scrollTo(0, 0)");
                Archive.WaitForElement(By.Id("UpdateZipcodeBtn"));
                updatezipCode.Click();
                Archive.WaitForElement(By.Id("zipCodeTextbox"));
                txt_zipCode.Clear();
                txt_zipCode.SendKeys(zipCode);
                btn_update[0].Click();
                Archive.WaitForPageLoad();
                Archive.WaitForElement(By.XPath("//span[.='You have changed the delivery zip-code, price(s) in your cart may have changed.']"));
                result = "updated Zipcode is done successfully";
            }
            catch (Exception ex)
            {
                result = "Failure in updating Zipcode" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Remove Product Items in Cart Page
        /// </summary>
        /// <returns></returns>
        public string removeItems()
        {
            try
            {
                btn_remove[0].Click();
                result = "Removing Items performed successfully";
            }
            catch (Exception ex)
            {
                result = "Failure in removing Items" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Move to Wish List from Cart Page
        /// </summary>
        /// <returns></returns>
        public string moveToWishList()
        {
            try
            {
                btn_moveToWishList[0].Click();
                result = "Moving to WishLists are performed successfully";
            }
            catch (Exception ex)
            {
                result = "Failure in Moving to wish list" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Click Proceed Check out at Cart Page
        /// </summary>
        /// <returns></returns>
        public string proceedToCheckout_CartPage()
        {
            try
            {
                Archive.WaitForElement(By.XPath("/descendant::span[contains(.,'Proceed to Checkout')]"));
                btn_checkOutCartPage.Click();
                Archive.WaitForPageLoad();
                result = "'Proceed to checkout' button is clicked successfully";

            }
            catch (Exception ex)
            {
                result = "'Proceed to Checkout' button is not clicked" + ex.Message + ex.StackTrace;
            }
            return result;
        }



        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }

}

