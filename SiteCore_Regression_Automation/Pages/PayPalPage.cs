﻿using AshleyAutomationLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class PayPalPage
    {
        public PayPalPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }
    
        [FindsBy(How = How.Id, Using = "email")]
        public IWebElement txt_payPalEmail;

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement txt_payPalPassword;

        [FindsBy(How = How.Id, Using = "btnLogin")]
        public IWebElement btn_payPalLoginButton;

        [FindsBy(How = How.Id, Using = "confirmButtonTop")]
        public IWebElement btn_payPalContinueButton;

        [FindsByAll]
        [FindsBy(How = How.XPath, Using = "//a[contains(@class,'edits')]")]
        public IList<IWebElement> lnk_Change;

        [FindsBy(How = How.XPath, Using = "//span[contains(@class,'leftText')]")]
        public IWebElement lnk_Add;

        [FindsBy(How = How.Id, Using = "firstName")]
        public IWebElement txt_firstName;

        [FindsBy(How = How.Id, Using = "lastName")]
        public IWebElement txt_lastName;

        [FindsBy(How = How.Id, Using = "shippingLine1")]
        public IWebElement txt_streetAddress1;

        [FindsBy(How = How.Id, Using = "shippingCity")]
        public IWebElement txt_city;

        [FindsBy(How = How.Id, Using = "shippingState")]
        public IWebElement slct_state;

        [FindsBy(How = How.Id, Using = "shippingPostalCode")]
        public IWebElement txt_zipCode;

        [FindsBy(How = How.Id, Using = "proceedButton")]
        public IWebElement btn_save;


    }
}
