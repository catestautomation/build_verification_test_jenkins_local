﻿using AshleyAutomationLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class StoreLocatorPage
    {

        public static string result = string.Empty;
        public StoreLocatorPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        [FindsBy(How = How.Id, Using = "address")]
        public IWebElement txt_Zip_location;

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'find-btn btn btn-default')]")]
        public IWebElement btn_Find;

        [FindsBy(How = How.XPath, Using = "//h1[.='Store Locator Results']")]
        public IWebElement txt_LocatoreResults;

        [FindsBy(How = How.XPath, Using = "//a[.='Store Details'] ")]
        public IWebElement Search_Result;

        [FindsBy(How = How.Id, Using = "imgChatTop2")]
        public IWebElement ele_LaunchOverlayClose;

        [FindsBy(How = How.Id, Using = "FindStoreTab")]
        public IWebElement dd_findAStore;

        [FindsBy(How = How.Id, Using = "StoreLocationListTab")]
        public IWebElement dd_storeLocationList;

        [FindsBy(How = How.ClassName, Using = "locationContent")]
        public IWebElement elm_locationInformation;

        [FindsBy(How = How.ClassName, Using = "distantContent")]
        public IWebElement elm_distanceInfo;
        
        [FindsBy(How = How.ClassName, Using = "selectCountryContent")]
        public IWebElement elm_countryInfo;

        [FindsBy(How = How.Id, Using = "FindStore")]
        public IWebElement elm_results;

        [FindsBy(How = How.ClassName, Using = "main")]
        public IWebElement elm_relatedSearchResults;

        [FindsBy(How = How.Id, Using = "map_canvas")]
        public IWebElement elm_map;

        [FindsBy(How = How.ClassName, Using = "searchResults")]
        public IWebElement elm_storeDetailsParent;

        [FindsBy(How = How.Id, Using = "descriptionStore")]
        public IWebElement elm_shopDetails;

        [FindsBy(How = How.Id, Using = "contentGoogleMaps")]
        public IWebElement elm_mapAtShopDetailPage;

        [FindsBy(How = How.ClassName, Using = "banner")]
        public IWebElement elm_banner;

        [FindsBy(How = How.Id, Using = "StoreLocationError")]
        public IWebElement elm_errorText;

        [FindsBy(How = How.Id, Using = "TbxLocation")]
        public IWebElement txt_location;

        [FindsBy(How = How.Id, Using = "Tbxdistance")]
        public IWebElement dd_distance;

        [FindsBy(How = How.Id, Using = "Tbxcountry")]
        public IWebElement dd_country;

        [FindsBy(How = How.ClassName, Using = "resultsForcountry")]
        public IWebElement elm_homeStoreResults;

        [FindsBy(How = How.Id, Using = "slc")]
        public IWebElement dd_storeLocationCountry;

        /// <summary>
        /// Navigate to Find Store page using string url
        /// </summary>
        /// <returns></returns>
        public string navigateToFindStore()
        {
            try
            {
                //string strUrl = "https://stores.ashleyfurniturehomestore.com/";
                string strUrl = "https://stores." + CommonFunctions.GetEnvironmentURL().Split('/')[2] + "/";
                Config.driver.Navigate().GoToUrl(strUrl);
                result = "Navigation to Store Locator Page is successful";
                Thread.Sleep(20000);
            }
            catch (Exception ex)
            {
                result = "Failure while navigating to Store Locator Page \n" + ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Send zip code in the find store locator page and Click find button
        /// </summary>
        /// <returns></returns>
        public string SendZipCodeandClick(string zipcode)
        {
            try
            {
                Archive.WaitForPageLoad();
                //if (Config.appUrl.Contains("stores.ashleyfurniturehomestore.com"))
                //{
                //    ele_LaunchOverlayClose.Click();                    
                //}
                //else
                //{
                //    TestContext.WriteLog("No thank you popup");
                //}
                Archive.WaitForElement(By.Id("address"));                
                txt_Zip_location.SendKeys(zipcode);
                txt_Zip_location.Clear();
                txt_Zip_location.SendKeys(zipcode);
                btn_Find.Click();
                result = "Entered zipcode and Find button clicked is successful";
                Archive.WaitForPageLoad();
            }
            catch (Exception ex)
            {
                result = "Failure in Entering zip or Clicking find button \n" + ex.Message;
            }
            return result;

        }
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;


    }
}
