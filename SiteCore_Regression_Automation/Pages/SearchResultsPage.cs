﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using SiteCore_Regression_Automation;
using AshleyAutomationLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Support.UI;

namespace SiteCore_Regression_Automation.Pages
{
    public class SearchResultsPage
    {
        public string result = string.Empty;
        public SearchResultsPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        [FindsBy(How = How.LinkText, Using = "My Account")]
        public IWebElement ele_myAccount;


        [FindsBy(How = How.Id, Using = "USI_toolbar_all")]
        public IWebElement ele_BottomOverlayClose;

        [FindsBy(How = How.Id, Using = "imgChatTop2")]
        public IWebElement ele_LaunchOverlayClose;

        [FindsBy(How = How.TagName, Using = "h1")]
        public IWebElement txt_headig;

        [FindsBy(How = How.Id, Using = "resultsContainer_products")]
        public IWebElement con_results;

        [FindsBy(How = How.ClassName, Using = "product-grid__list")]
        public IWebElement con_subCategoryResults;

        [FindsBy(How = How.ClassName, Using = "product-grid__footer")]
        public IWebElement con_resultCountInfo;

        [FindsBy(How = How.Id, Using = "TbxSearch")]
        public IWebElement txt_searchInputField;

        [FindsBy(How = How.Id, Using = "ctl09_searchIcon2")]
        public IWebElement btn_searchInputButton;

        [FindsBy(How = How.ClassName, Using = "product-more-options")]
        public IWebElement btn_productmoreOption;

        [FindsBy(How = How.Id, Using = "checkPriceZipCode")]
        public IWebElement txt_zipCodeSideBar;

        [FindsBy(How = How.ClassName, Using = "overlay__container")]
        public IWebElement con_zipcodeContainer;

        [FindsBy(How = How.Id, Using = "quickViewCheckPriceModal")]
        public IWebElement con_quickViewZipCodeContainer;

        [FindsBy(How = How.Id, Using = "addToCart")]
        public IWebElement con_productLightBox;

        [FindsBy(How = How.Id, Using = "addToCartQuickView")]
        public IWebElement btn_addToCartButtonQuickView;

        [FindsBy(How = How.ClassName, Using = "accordion subCategory facets_container")]
        public IWebElement con_leftSidePriceFilterContainer;

        [FindsBy(How = How.Id, Using = "price-filterr")]
        public IWebElement fltr_leftSidepriceFilter;

        [FindsBy(How = How.Id, Using = "clear_all_selected_facets")]
        public IWebElement btn_clearAll;

        [FindsBy(How = How.Id, Using = "productsSortBy")]
        public IWebElement dd_sortByDropdown;

        public String String_dd_sortByDropdown = "productsSortBy";

        [FindsBy(How = How.ClassName, Using = "product-facets__header")]
        public IWebElement btn_subCategoryClearAllParent;

        [FindsBy(How = How.ClassName, Using = "logo")]
        public IWebElement img_AshleyLogo;

        [FindsByAll]
        [FindsBy(How = How.XPath, Using = "//a[@class='product-grid-item__link']")]
        public IList<IWebElement> searchResultProductCount;

        [FindsBy(How = How.ClassName, Using = "product-price")]
        public IList<IWebElement> div_productPrice;

        [FindsBy(How = How.ClassName, Using = "product-price__check-price")]
        public IList<IWebElement> lnk_checkPrice;

        [FindsBy(How = How.XPath, Using = "/descendant::a[.= 'Check price'][1] | /descendant::span[.= 'Check price'][1]")]
        public IWebElement btn_CheckPrice;

        [FindsBy(How = How.ClassName, Using = "zip-code-overlay__input")]
        [FindsBy(How = How.Id, Using = "checkPriceModalInput")]
        public IWebElement txt_zipCodeOverlay;

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'zip-code-overlay__button')] | //span[@class='input-group-addon button postfix fa fa-search zipCodeGlass']")]
        public IWebElement btn_zipCodeOverlayButton;

        [FindsBy(How = How.ClassName, Using = "product-swatches")]
        public IList<IWebElement> con_productSwatch;

        [FindsBy(How = How.LinkText, Using = "To View Local Pricing")]
        public IWebElement lnk_ToViewLocalPricing;

        [FindsBy(How = How.ClassName, Using = "product-zip-code__content-link")]
        public IWebElement lnk_EnterYourZIPCode;

        [FindsBy(How = How.ClassName, Using = "product-grid-item__content")]
        public IWebElement searchResultCount;

        [FindsByAll]
        [FindsBy(How = How.XPath, Using = "//span[contains(@class,'product-price') and contains(.,'$')]")]
        public IList<IWebElement> productPriceCount;
      
        [FindsBy(How = How.Id, Using = "productsSortBy")]
        public IWebElement dd_SortBy;

        [FindsBy(How = How.TagName, Using = "svg")]
        public IWebElement spinner_searchResultsPage;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "product-grid-item__title")]
        public IList<IWebElement> title_ProductGridItem;

        [FindsBy(How = How.ClassName, Using = "product-grid__list")]
        public IWebElement img_ProductList;

        [FindsBy(How = How.ClassName, Using = "product-facets__title")]
        public IWebElement fltr_NarrowByHeader;

        [FindsBy(How = How.ClassName, Using = "product-facets")]
        public IWebElement con_Filter;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "facet-item__link")]
        public IList<IWebElement> chk_FilterOptions;        

        [FindsBy(How = How.PartialLinkText, Using = "Click to View All Products")]
        public IWebElement btn_ClickToViewAllProducts;

        [FindsBy(How = How.LinkText, Using = "Sign In")]
        public IWebElement btn_SignIn;

        [FindsBy(How = How.Id, Using = "ctl10_accountIcon")]
        public IWebElement btn_MyAccount;
  
        [FindsByAll]
        [FindsBy(How = How.TagName, Using = "span",Priority =1)]
        [FindsBy(How = How.ClassName, Using = "product-facets__clear-all", Priority=0)]
        public IWebElement optn_ClearAll;

        [FindsBy(How = How.ClassName, Using = "product-grid-item__shipping-label")]
        public IWebElement lbl_ProductShippingOption;

        [FindsBy(How = How.PartialLinkText, Using = "$250-$500")]
        public IWebElement chk_ProductPriceOption1;
        
        [FindsBy(How = How.Id, Using = "$250-$500")]
        public IWebElement chk_ProductOption;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "facet-group__list")]
        public IList<IWebElement> filter_Options;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "facet-group__title")]
        public IList<IWebElement> filter_Title;

        [FindsBy(How = How.ClassName, Using = "product-grid__showing-items")]
        public IWebElement footer_ProductCount;

        [FindsBy(How = How.Id, Using = "tab_products")]
        public IWebElement productCountInHeader;

        [FindsBy(How = How.PartialLinkText, Using = "Search Results For \"Stool\"")]
        public IWebElement searchResultInformation;

        [FindsBy(How = How.ClassName, Using = "prodThumbResultCountTop displayPrdCount")]
        public IWebElement ProductCountFromSearchInformation;



        public string navigateToSearchResultsPage()
        {
            try
            {
                //String SearchResultsPageURL = "http://ashcomm9.cds.ashleyretail.com/searchpage?s=Sofas#fndtn-tabSearch_products";
                string SearchResultsPageURL = Config.appUrl+"searchpage?s=Sofas#fndtn-tabSearch_products";
                Config.driver.Navigate().GoToUrl(SearchResultsPageURL);
                Archive.WaitForPageLoad();
                result = "Navigation to PDPPage is successful";
            }
            catch (Exception ex)
            {
                result = "Failure in navigation to PDPPage \n" + ex.Message;
            }
            return result;
        }

        public string navigateToSubCategoryPage()
        {
            try
            {
                //String SubCategoryPageURL = "http://ashcomm9.cds.ashleyretail.com/c/furniture/living-room/sofas/?sortby=featured&icid=meganav-furniture-menu-sofas";
                string SubCategoryPageURL = Config.appUrl+"c/furniture/living-room/sofas/?sortby=featured&icid=meganav-furniture-menu-sofas";
                Config.driver.Navigate().GoToUrl(SubCategoryPageURL);
                Archive.WaitForPageLoad();
                result = "Navigation to PDPPage is successful";
            }
            catch (Exception ex)
            {
                result = "Failure in navigation to PDPPage \n" + ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Enter ZIPCode in ZIPCode popup
        /// </summary>
        /// <param name="strZIPCode"></param>
        /// <param name="strLogDescription"></param>
        /// <returns></returns>
        public bool enterZIPCodeInSearchResultsPageZIPCodePopUp(String strZIPCode, String strLogDescription)
        {
            bool isEntered = false;
            try
            {
                txt_zipCodeOverlay.SendKeys(strZIPCode);
                btn_zipCodeOverlayButton.Click();
                isEntered = true;
            }
            catch (Exception e)
            {
                isEntered = false;
                System.Console.WriteLine(e.Message);
                System.Console.WriteLine("FAILED:" + strLogDescription);
            }

            return isEntered;
        }

        /// <summary>
        /// Search product from SearchResultsPage
        /// </summary>
        /// <param name="IsQuickView"></param>
        /// <param name="IsSearchResults"></param>
        /// <returns></returns>
        public string selectProductFromSearchResultsPage(bool IsQuickView = false, bool IsSearchResults = false)
        {
            try
            {
                if (IsSearchResults == false)
                {
                    Archive.WaitForElement(By.ClassName("product-grid__list"));
                    List<IWebElement> list_Products = con_subCategoryResults.FindElements(By.ClassName("aspect-ratio-image__image")).ToList();
                    if (IsQuickView == false)
                    {
                        list_Products[0].Click();
                        Archive.WaitForPageLoad();
                    }
                    else
                    {
                        list_Products[0].MouseOver();
                        Thread.Sleep(2000);
                        List<IWebElement> quickViewlist_Products = con_subCategoryResults.FindElements(By.ClassName("product-grid-item__image-button")).ToList();
                        quickViewlist_Products[0].Click();
                        Archive.WaitForPageLoad();
                    }
                }
                else
                {
                    Archive.WaitForElement(By.Id("resultsContainer_products"));
                    List<IWebElement> list_Products = con_results.FindElements(By.ClassName("thumbnailImage")).ToList();
                    if (IsQuickView == false)
                    {
                        list_Products[0].Click();
                        Archive.WaitForPageLoad();
                    }
                    else
                    {
                        list_Products[0].MouseOver();
                        List<IWebElement> quickViewlist_Products = con_results.FindElements(By.ClassName("button secondary tiny quickView")).ToList();
                        quickViewlist_Products[0].Click();
                        Archive.WaitForPageLoad();
                    }
                }
                result = "Selecting Product from search reults page is Succesful - QuickView selection - " + IsQuickView;
            }
            catch (Exception ex)
            {
                result = "Failure in Selecting Product from search reults page - QuickView selection" + IsQuickView + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Clicking Check price and Entering ZipCode in product SearchResultsPage
        /// </summary>
        /// <param name="zipCode"></param>
        /// <returns></returns>


        /// <summary>
        /// check price click in product SearchResultsPage
        /// </summary>
        /// <param name="zipCode"></param>
        /// <returns></returns>
        public string checkPriceSearchResultsPage(string zipCode)
        {
            SearchResultsPage SearchResultsPage = new SearchResultsPage();
            try
            {
                //Archive.WaitForElement(By.ClassName("product-price__check-price"));
                //Archive.WaitForElement(By.LinkText("Check price"));
                //Archive.WaitForElement(By.XPath("//a[.='Check price']"));

                Archive.WaitForElement(By.XPath("/descendant::a[.= 'Check price'][1] | /descendant::span[.= 'Check price'][1]"));
                btn_CheckPrice.MouseOver();
                btn_CheckPrice.Click();
                //int priceCount = div_productPrice.Count;
                //for (int i = 0; i < priceCount; i++)
                //{
                //    List<IWebElement> productPriceChildren = div_productPrice[i].GetDescendants();
                //    if (lnk_checkPrice[i].Text == "Check price")
                //    {
                //        lnk_checkPrice[i].Click();
                //        Archive.WaitForPageLoad();
                //        break;
                //    }
                //}
                Archive.WaitForElement(By.XPath("//input[@id='checkPriceModalInput'] | //input[@class='zip-code-overlay__input']"));
                txt_zipCodeOverlay.SendKeys(zipCode);
                btn_zipCodeOverlayButton.Click();
                Thread.Sleep(3000);
                Archive.WaitForPageLoad();
                Config.driver.Navigate().Refresh();
                Archive.WaitForPageLoad();
                result = "Clicking check price and entering Zipcode in search results page is succesful";
            }
            catch (Exception ex)
            {
                result = "Failure in selecting check price and entering Zipcode in search results page" + ex.Message + ex.StackTrace;
            }

            return result;

        }


        /// <summary>
        /// Clicking Color swatch in SearchResultsPage
        /// </summary>
        /// <returns></returns>
        public string colorSwatchInSearchResultsPage()
        {
            try
            {
                List<IWebElement> colorSwatch = con_productSwatch[0].FindElements(By.TagName("img")).ToList();
                colorSwatch[0].MouseOver();
                colorSwatch[0].Click();
                Archive.WaitForPageLoad();
            }
            catch (Exception ex)
            {
                result = "Error :" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Search product validation with search keyword
        /// </summary>
        /// <returns></returns>
        public string searchResultsValidationBasedOnProductName()
        {
            try
            {
                //product count
                string[] strSplitFromInformation = ProductCountFromSearchInformation.Text.Split('f');
                String strActualCountFromSearchInfo = strSplitFromInformation[1].Trim();

                //product count from header
                String[] strCount = productCountInHeader.Text.Split(')');
                String str1 = strCount[0];
                String str2 = strCount[1];
                //String str3 = strCount[0];
                String strCountFinal = strCount[0].Replace("(", "");

                if (strActualCountFromSearchInfo.Equals(strCountFinal))
                {
                    result = "Search validation based on product name is successful";
                }
                else
                {
                    throw new Exception("Product count mismatch");
                }
            }
            catch (Exception ex)
            {
                result = "Failure in search validation:" + ex.Message + ex.StackTrace;
            }
            return result;
        }


        /// <summary>
        /// For filter options click in SearchResultsPage
        /// </summary>
        /// <param name="lstFilterOptions"></param>
        /// <param name="strFilterOption"></param>
        /// <returns></returns>
        public string filterOptionClick(IList<IWebElement> lstFilterOptions, String strFilterOption)
        {
            try
            {
                IJavaScriptExecutor jse = (IJavaScriptExecutor)Config.driver;
                jse.ExecuteScript("window.scrollBy(0,-3000)", "");
                Archive.WaitForElement(By.ClassName("facet-item__link"));
                int filterOption = lstFilterOptions.IndexOf(lstFilterOptions.First(x => x.Text.Contains(strFilterOption)));
                lstFilterOptions[filterOption].MouseOver();
                lstFilterOptions[filterOption].Click();
                var wait = new WebDriverWait(Config.driver, TimeSpan.FromSeconds(30));
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[contains(@class,'spinner')]")));
                result = "Click on filter option is successful";
            }
            catch (Exception ex)
            {
                result = "Failure in clicking filter option" + ex.Message + ex.StackTrace;
            }
            return result;
        }


        /// <summary>
        /// Verify Narrow by filter options
        /// </summary>
        /// <param name="lst_filter_Title"></param>
        /// <param name="lst_filter_Options"></param>
        /// <returns></returns>
        public string verifyFilterOptions(IList<IWebElement> lst_filter_Title, IList<IWebElement> lst_filter_Options)
        {
            bool isVerified = false;

            try
            {
                Archive.WaitForElement(By.ClassName("facet-group__title"));
                if (lst_filter_Title.Count == lst_filter_Options.Count)
                {
                    int i = lst_filter_Title.Count;
                    int j = lst_filter_Options.Count;
                    result = "Success in filter and its option validation";
                }
                else
                {
                    throw new Exception("Count mismatch in filter title and filter options");
                }

            }
            catch (Exception ex)
            {
                isVerified = false;
                result = "Failure in Filter and its options verification" + ex.Message + ex.StackTrace;
            }
            return result;
        }


        /// <summary>
        /// Enter ZIPCode in ZIPCode popup
        /// </summary>
        /// <param name="strZIPCode"></param>
        /// <param name="strLogDescription"></param>
        /// <returns></returns>
        public string enterZIPCodeInSearchResultsPageZIPCodePopUp(String strZIPCode)
        {
            bool isEntered = false;
            try
            {
                Archive.WaitForElement(By.XPath("//input[@id='checkPriceModalInput'] | //input[@class='zip-code-overlay__input']"));
                txt_zipCodeOverlay.SendKeys(strZIPCode);
                btn_zipCodeOverlayButton.Click();
                Archive.WaitForPageLoad();
                result = "Entering Zipcode in search results page is succesful";
            }
            catch (Exception ex)
            {
                result = "Failure in selecting check price and entering Zipcode in search results page" + ex.Message + ex.StackTrace;
            }

            return result;
        }


        /// <summary>
        /// check price click in product SearchResultsPage
        /// </summary>
        /// <param name="zipCode"></param>
        /// <returns></returns>
        /// 

        //public string checkPriceSearchResultsPage(string zipCode)
        //{
        //    SearchResultsPage SearchResultsPage = new SearchResultsPage();
        //    try
        //    {
        //        Archive.WaitForElement(By.XPath("/descendant::a[.= 'Check price'][1] | /descendant::span[.= 'Check price'][1]"));
        //        btn_CheckPrice.Click();
        //        Archive.WaitForElement(By.XPath("//input[@id='checkPriceModalInput'] | //input[@class='zip-code-overlay__input']"));
        //        txt_zipCodeOverlay.SendKeys(zipCode);
        //        btn_zipCodeOverlayButton.Click();
        //        Archive.WaitForPageLoad();
        //        result = "Clicking check price and entering Zipcode in search results page is succesful";
        //    }
        //    catch (Exception ex)
        //    {
        //        result = "Failure in selecting check price and entering Zipcode in search results page" + ex.Message + ex.StackTrace;
        //    }

        //    return result;

        //}


        /// <summary>
        /// Enter ZIPCode under "ToViewAllLocalPricing"
        /// </summary>
        /// <returns></returns>
        public string clickEnterZipCodeUnderToViewAllLocalPricing()
        {
            try
            {
                Archive.WaitForPageLoad();
                Archive.WaitForElement(By.ClassName("product-zip-code__content-link"));
                lnk_EnterYourZIPCode.MouseOver();
                lnk_EnterYourZIPCode.Click();
                result = "Click ZipCode button under To View All Local Pricing is successful";

            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
                result = "Failure in Clicking ZipCode button under To View All Local Pricing";
            }
            return result;
        }


        /// <summary>
        /// Verify the order of the product list based on the dropdown
        /// </summary>
        /// <param name="ele_ProductCount"></param>
        /// <param name="IsAscendingOrder"></param>
        /// <param name="IsDescendingOrder"></param>
        /// <param name="isAlphabeticalOrder"></param>
        /// <returns></returns>
        public string searchResultOrderVerification(IList<IWebElement> ele_ProductCount, bool IsAscendingOrder = false, bool IsDescendingOrder = false, bool isAlphabeticalOrder = false)
        {
            bool isVerified = false;
            try
            {
                Archive.WaitForElement(By.ClassName("product-grid__list"));
                var wait = new WebDriverWait(Config.driver, TimeSpan.FromSeconds(30));
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div[contains(@class,'spinner')]")));
                List<String> lstProductFinalList = new List<String>();
                for (int i = 0; i < ele_ProductCount.Count; i++)
                {
                    if (ele_ProductCount[i].GetAttribute("class").Contains("old"))
                    {
                        System.Console.WriteLine("Skipped>>" + ele_ProductCount[i].Text);
                    }
                    else
                    {
                        lstProductFinalList.Add(ele_ProductCount[i].Text);
                        System.Console.WriteLine("The data is>>" + ele_ProductCount[i].Text);
                    }
                }
                if ((IsAscendingOrder == true) || (isAlphabeticalOrder == true))
                {
                    isVerified = CommonFunctions.verifyAscendingOrder(lstProductFinalList);
                    if (isVerified == true)
                    {
                        result = "The product order verfication is successful";
                    }
                    else
                    {
                        throw new Exception("Failure in product order verfication");
                    }
                }
                else if (IsDescendingOrder == true)
                {
                    isVerified = CommonFunctions.verifyDescendingOrder(lstProductFinalList);
                    if (isVerified == true)
                    {
                        result = "The product order verfication is successful";
                    }
                    else
                    {
                        throw new Exception("Failure in product order verfication");
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Verify element displayed in the leftside
        /// </summary>
        /// <param name="elmntOne"></param>
        /// <param name="elmntTwo"></param>
        /// <returns></returns>
        public string verifyElementDisplayedInLeftSide(IWebElement elmntOne, IWebElement elmntTwo)
        {
            try
            {
                System.Console.WriteLine(elmntOne.Location.X);
                System.Console.WriteLine(elmntTwo.Location.X);
                if ((elmntOne.Location.X) < (elmntTwo.Location.X))
                {
                    result = "Element displayed in the leftside validation is successful";
                }
                else
                {
                    throw new Exception("Failed in element location validation");
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Verify product price based on the filter in SearchResultsPage
        /// </summary>
        /// <param name="lstProductList"></param>
        /// <param name="producePriceFilterOption"></param>
        /// <returns></returns>
        public string verifyProductPriceBasedOnFilterInSearchResultspage(IList<IWebElement> lstProductList, String producePriceFilterOption)
        {
            bool isVerified = false;
            try
            {
                Archive.WaitForElement(By.XPath("//span[contains(@class,'product-price') and contains(.,'$')]"));
                String[] price = producePriceFilterOption.Split('-');
                List<String> lstProductFinalList = new List<String>();
                for (int i = 0; i < lstProductList.Count; i++)
                {
                    if (lstProductList[i].GetAttribute("class").Contains("old"))
                    {
                        System.Console.WriteLine("Skipped>>" + lstProductList[i].Text);
                    }
                    else
                    {
                        lstProductFinalList.Add(lstProductList[i].Text);
                        System.Console.WriteLine("The data is>>" + lstProductList[i].Text);
                    }

                }
                for (int j = 0; j < lstProductFinalList.Count; j++)
                {
                    if (float.Parse(lstProductFinalList[j].Replace("$", "").Trim()) >= float.Parse(price[0].Replace("$", "").Trim()) && float.Parse(lstProductFinalList[j].Replace("$", "").Trim()) <= float.Parse(price[1].Replace("$", "").Trim()))
                    {
                        result = "product price verification is successful";
                    }
                    else
                    {
                        throw new Exception("Failure in product price verification");
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Select value from SortBy dropdown
        /// </summary>
        /// <param name="selectOption"></param>
        /// <returns></returns>
        public String selectValueFromSortByDropdown(String selectOption)
        {
            try
            {
                Archive.WaitForElement(By.Id("productsSortBy"));
                SelectElement sel = new SelectElement(dd_SortBy);
                Thread.Sleep(2000);
                sel.SelectByText(selectOption);
                result = selectOption + "value selected from dropdown";
            }
            catch (Exception ex)
            {
                result = "Failure in selecting" + selectOption + "value selected from dropdown" + ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Verify product count after filter selection
        /// </summary>
        /// <param name="productCount"></param>
        /// <returns></returns>
        public string productCountVerificationAfterFilterSelection(int productCount)
        {
            try
            {
                Archive.WaitForElement(By.XPath("//a[@class='product-grid-item__link']"));
                //List<IWebElement> lst_ProductCount = Config.driver.FindElements(By.XPath(SearchResultsPage.searchResultProductCount)).ToList();
                if (searchResultProductCount.Count == productCount)
                {
                    result = "Product count verification is successful";
                }
                else
                {
                    throw new Exception("Failure in product count validation is successful");
                }

            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Verify product count defined in the filter option
        /// </summary>
        /// <param name="PriceFilterValue"></param>
        /// <returns></returns>
        public string verifyProductCountBasedOnFilterOptionSelection(String PriceFilterValue)
        {
            try
            {
                String strExpectedOld = Config.driver.FindElement(By.XPath("//label[@for='<<REPLACECONTENT>>']/span".Replace("<<REPLACECONTENT>>", PriceFilterValue))).Text;
                String strExpected = strExpectedOld.Replace("(", "").Replace(")", "");
                string[] strSplit = footer_ProductCount.Text.Split('f');
                String strActual = strSplit[1].Trim();
                if (strExpected.Equals(strActual))
                {
                    result = "product count validation based on filter option is successful";
                }
                else
                {
                    throw new Exception("Failure in product count validation based on filter option");
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Product name verification based on filter selection
        /// </summary>
        /// <param name="filterOption"></param>
        /// <returns></returns>
        public string verifyProductNameBasedOnFilterOption(string filterOption)
        {
            try
            {
                Archive.WaitForElement(By.ClassName("product-grid-item__title"));
                for (int m = 0; m < title_ProductGridItem.Count; m++)
                {
                    if (title_ProductGridItem[m].Text.Contains(filterOption))
                    {
                        result = "Search result validation is successful";
                    }
                    else
                    {
                        throw new Exception("Failure in search result validation");
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Click on clear all option
        /// </summary>
        /// <returns></returns>
        public string clickOnClearAllOption()
        {
            try
            {
                Archive.WaitForElement(By.ClassName("product-facets__clear-all"));
                optn_ClearAll.Click();
                result = "Click on clear all option is successful";
            }
            catch (Exception ex)
            {
                result = "Failure in clear all option click";
            }
            return result;
        }

        /// <summary>
        /// Click on first Narrow by filter title
        /// </summary>
        /// <returns></returns>
        public string clickOnFirstNarrowByFilterTitle()
        {
            try
            {
                Archive.WaitForElement(By.ClassName("facet-group__title"));
                filter_Title[0].Click();
                result = "NarrowBy filter title click is successful";
            }
            catch (Exception ex)
            {
                result = "failure" + "in NarrowBy filter title click" + ex.Message + ex.StackTrace;
            }
            return result;
        }
    }



}





