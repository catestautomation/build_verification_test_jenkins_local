﻿using AshleyAutomationLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class ThankYouPage
    {
        public ThankYouPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }
        public static string result = string.Empty;

        [FindsBy(How = How.ClassName, Using = "order-confirmation-page")]
        public IWebElement elm_orderConfirmationHeader;

        [FindsBy(How = How.ClassName, Using = "desciption thank-you-text-wrapper")]
        public IWebElement lbl_thankYouPageContent;

        [FindsBy(How = How.ClassName, Using = "brdialog-close")]
        public IWebElement btn_orderConfirmCloseOverlay;

        [FindsBy(How = How.Id, Using = "scphbody_0_CreateGuestCustomer_UserCreatePassword")]
        public IWebElement txt_createPassword;

        [FindsBy(How = How.Id, Using = "scphbody_0_CreateGuestCustomer_UserConfirmPassword")]
        public IWebElement txt_confirmPassword;

        [FindsBy(How = How.Id, Using = "scphbody_0_CreateGuestCustomer_UserSubmitButton")]
        public IWebElement btn_createAccount;

        [FindsBy(How = How.ClassName, Using = "msax-AddressView")]
        public IWebElement elm_addressViewAtThankYouPage;

        [FindsBy(How = How.ClassName, Using = "secure-tile-content")]
        public IWebElement elm_secureContent;

        [FindsBy(How = How.Id, Using = "stateRecyclingFee")]
        public IWebElement con_recyclingFeePopUp;

        [FindsBy(How = How.XPath, Using = "//span[(@class='DateSelected')]")]
        public IWebElement ele_selectedDate;

        


        //public static object con_productDetails = new { Class = "ProductDetails",TagName="td" };
        [FindsBy(How = How.ClassName, Using = "ProductDetails")]
        public IWebElement con_productDetails;

        /// <summary>
        /// Account creation in ThankYouPage
        /// </summary>
        /// <param name="password"></param>
        /// <param name="confirmPassowrd"></param>
        /// <returns></returns>
        public string createAccountThankYouPage(String password, String confirmPassowrd)
        {
            try
            {
                txt_createPassword.SendKeys(password);
                txt_confirmPassword.SendKeys(confirmPassowrd);
                btn_createAccount.Click();
                Archive.WaitForPageLoad();
            }
            catch (Exception ex)
            {
                result = "Error :" + ex.Message + ex.StackTrace;
            }

            return result;
        }

        public string orderConfirmOverlayClick()
        {
            try
            {
                Archive.WaitForElement(By.ClassName("brdialog-close"));

                new Actions(Config.driver).MoveToElement(btn_orderConfirmCloseOverlay).Click().Perform();
                result = "'Order Confirm Overlay' has been closed";
            }
            catch (Exception ex)
            {
                result = "Failure: 'Order confirm Overlay' is not closed" + ex.Message + ex.StackTrace;
            }
            return result;
        }

    }
}
