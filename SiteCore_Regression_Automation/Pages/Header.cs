﻿using AshleyAutomationLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class Header
    {

        public string result = string.Empty;
        public Header()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        [FindsBy(How = How.Id, Using = "ctl09_afhsLogoLink")]
        public IWebElement img_ashleyLogo;

        [FindsBy(How = How.ClassName, Using = "search-box")]
        public IWebElement elm_subCategorySearchBox;

        [FindsBy(How = How.Id, Using = "ctl09_searchIcon2")]
        public IWebElement btn_searchInputButton;

        [FindsBy(How = How.Id, Using = "ctl09_cartIcon")]
        public IWebElement img_cartIcon;

        [FindsBy(How = How.Id, Using = "afhs-account-dropdown")]
        public IWebElement drpdwn_dropDownParent;

        [FindsBy(How = How.TagName, Using = "a")]
        public IWebElement drpdwn_lists;

        [FindsBy(How = How.ClassName, Using = "menu-dropdown__item")]
        public IWebElement dropdwn_list;
        
        [FindsBy(How = How.ClassName, Using = "afhs-top-menu")]
        public IWebElement elm_topMenu;

        [FindsBy(How = How.ClassName, Using = "unbxd-as-wrapper unbxd-as-extra-right")]
        public IWebElement elm_searchSuggestionDDL;

        [FindsBy(How = How.PartialLinkText, Using = "Help")]
        public IWebElement lnk_Help;

        [FindsBy(How = How.ClassName, Using = "afhs-logo-img")]
        public IWebElement icn_AshleyHomeStore;

        [FindsBy(How = How.Id, Using = "TbxSearch")]
        [FindsBy(How = How.ClassName, Using = "search-box__input")]
        public IWebElement txt_searchInputField;

        //public object txt_searchInputField = new { Class = "search-box__input" };
        // public object btn_searchInputbutton = new { Id = "ctl10_searchIcon2" };//
        [FindsBy(How = How.ClassName, Using = "search-box__icon")]
        [FindsBy(How = How.XPath, Using = "//div[@id='afhsUnbxd']/descendant::img[contains(@id,'searchIcon')]")]
        public IWebElement btn_searchInputbutton;

        [FindsBy(How = How.Id, Using = "ctl09_accountIcon")]   //Ashcomm5
        [FindsBy(How = How.Id, Using = "ctl10_accountIcon")]   //Ashcomm9
        public IWebElement img_myAccount;

        /// <summary>
        /// Search a Product in Search field
        /// </summary>
        /// <param name="productname"></param>
        /// <returns></returns>
        public string searchForProduct(String productname)
        {
            try
            {           
                txt_searchInputField.SendKeys(productname);
                btn_searchInputbutton.Click();
                Archive.WaitForPageLoad();                
            }
            catch (Exception ex)
            {
                result = "Error :" + ex.Message + ex.StackTrace;
            }

            return result;

        }

        /// <summary>
        /// My Account Option Dropdown has been selected
        /// </summary>
        /// <param name="dropDown"></param>
        /// <returns></returns>
        public string myAccountOption(string dropDown)
        {
            try
            {
                // Archive.WaitForElement(By.XPath("//div[contains(@class,'homepage')]//img[contains(@id,'accountIcon')]"));                                
                img_myAccount.MouseOver();
                Archive.WaitForElement(By.Id("afhs-account-dropdown"));
                List<IWebElement> drpdwn_list = drpdwn_dropDownParent.GetDescendants();
                int option_Index = drpdwn_list.IndexOf(drpdwn_list.First(x => x.Text.Contains(dropDown)));
                drpdwn_list[option_Index].Click();
                Archive.WaitForPageLoad();
            }
            catch (Exception ex)
            {
                result = "Error :" + ex.Message + ex.StackTrace;
            }

            return result;
        }


    }
}
